;
(function ($, window, document, undefined) {
    $.fn.retheme_modal = function (args) {

        return this.each(function () {

            var element = $(this);

            var defaults = {
                animatein: 'transition.fadeIn',
                animateout: 'transition.fadeOut',
                duration: '300',
                easing: 'easeIn',
            };

            var meta = element.data();
            var options = $.extend(defaults, args, meta);

            var selector = {
                close: '.js-modal-close',
            }



            var open = function () {

                element.velocity('transition.fadeIn');
                /** modal inner show */
                element.find('.rt-modal__inner').velocity(options.animatein, {
                    duration: options.duration,
                    easing: options.easing,
                }).addClass('is-active');

                $("html").css("overflow-y", "hidden");
            }

            var close = function () {
                element.velocity('reverse', {
                    display: 'none',
                });
                /** modal inner hidden */
                element.find('.rt-modal__inner').velocity(options.animateout, {
                    duration: options.duration,
                    easing: options.easing,
                }).removeClass('is-active');

                $('html').css('overflow-y', 'scroll');
            }


            if (options.action == 'open'){
                open();
            }

            /**
             * Modal action on click
             */
            $(options.trigger).on('click', function (event) {
                event.preventDefault();
                open();
            });

            /**
             * hidden if overlay back click
             */
            $(selector.close).on("click", function(event) {
              event.preventDefault();
              close();
            });     


        });
    };
})(jQuery, window, document);