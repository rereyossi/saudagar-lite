(function($, window, document, undefined) {
  $.fn.retheme_sidepanel = function(args) {
    return this.each(function() {
      var element = $(this);

      var defaults = {
        animatein: "transition.slideBigIn",
        animateout: "transition.slideBigOut",
        duration: 500,
        easing: "easeIn",
        active: false
      };

      var meta = element.data();
      var options = $.extend(defaults, args, meta);

      var selector = {
        trigger: ".js-sidepanel-trigger",
        close: ".js-sidepanel-close",
        panel: ".rt-sidepanel__inner",
        overlay: ".rt-sidepanel__overlay"
      };

      /**
       * Check meta trigger null or not
       */
      if (options.trigger) {
        var trigger = $(options.trigger);
      } else {
        var trigger = $(selector.trigger);
      }

      /**
       * Open panel
       */
      // check panel position
       var position = {
         translateX: ["0%", "100%"]
       };

      if (element.hasClass("rt-sidepanel--left")) {
        var position = {
          translateX: ["0%", "-100%"],
        };
      }

       if (element.hasClass("rt-sidepanel--bottom")) {
         var position = {
           translateY: ["0%", "100%"]
         };
       }
      

      var open = function() {
        
        element.velocity("transition.fadeIn");
        
        $(selector.panel).velocity("stop").velocity(position,
          {
            duration: options.duration,
            easing: options.easing,
            display: "block"
          }
        );
        trigger.addClass("is-active");
        $("html").css("overflow-y", "hidden");
      };

      /**
       * Close panel
       */
      var close = function() {
        element.velocity("reverse", { display: "none" });
        $(selector.panel).velocity("reverse", {
          display: "none"
        });
        trigger.removeClass("is-active");
        $("html").css("overflow-y", "scroll");
      };

      /**
      * set default panel active
      */
      if (options.action == "open") {
        open();
      }else{
           /**
       * set active with trigger
       */
      trigger.on("click", function(event) {
        event.stopPropagation();
        
        open();
       
      });
      }
   

      /**
       * Close side panel and backdrop
       */
      $(selector.close).click(function(event) {
        event.stopPropagation();
        close();
        
      });

     
    });
  };
})(jQuery, window, document);
