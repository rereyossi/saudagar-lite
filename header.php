<!DOCTYPE html>

<html class="no-js" <?php language_attributes() ?>>
  <head>
     <?php wp_head();?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

  </head>
  <body <?php body_class(); ?>>

    <?php do_action('rt_before_main') ?>

    <div id="page-main" class="page-main">
    
        <?php do_action( 'rt_header') ?>