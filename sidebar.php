<?php
$classes = array();

if (rt_option('sidebar_option_sticky', true)) {
  $classes[] = 'js-aside-sticky';
}
 ?>
  <aside id="page-aside" <?php rt_set_class('rt_page_aside_class', array('page-aside')) ?>>

          <div <?php rt_set_class('rt_aside_inner_class', $classes) ?>>
          
               <?php  do_action('rt_sidebar_widget'); ?>

          </div>

  </aside>