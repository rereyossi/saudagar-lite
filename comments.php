
<?php

if (post_password_required()) {
    return;
}

function rt_comment_callback($comment, $args, $depth)
{
    if ('div' === $args['style']) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    } ?>
    <<?php echo $tag ?> <?php comment_class(empty($args['has_children']) ? '' : 'parent')?> id="comment-<?php comment_ID()?>">
    
    <?php if ('div' != $args['style']): ?>
        <div id="div-comment-<?php comment_ID()?>">
    <?php endif; ?>

   <div class="rt-comment-list__item">
      <div class="rt-img rt-comment-list__avatar">
          <?php echo get_avatar($comment, $args['avatar_size']); ?>
      </div>
        <div class="rt-comment-list__body">

            <?php if ($comment->comment_approved == '0'): ?>
              <p class="rt-comment-list__approved"><?php _e('Your comment is awaiting moderation.', 'rt_domain'); ?></p>
            <?php endif; ?>

            <h5 class="rt-comment-list__title">
              <a href="<?php echo get_comment_author_url(); ?>"><?php echo get_comment_author() ?></a>
              <?php if (get_the_author() == get_comment_author()): ?>
                <span class="rt-comment-list__author"><?php _e('author', 'rt_domain') ?></span>
              <?php endif; ?>
            </h5>

            <div class="rt-comment-list__meta">
              <span href="#" class="comment-list-meta-date"><i class="fa fa-clock-o" aria-hidden="true"></i>
                <?php printf(_x('%1$s at %2$s', '1: date, 2: time', 'rt_domain'), get_comment_date(), get_comment_time()); ?>
              </span>
            </div>

            <div class="rt-comment-list__content">
                <?php comment_text(); ?>
            </div>

            <div class="rt-comment-list__reply">
              <i class="fa fa-reply"></i> <?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
            </div>

        </div>
    </div>

    <?php if ('div' != $args['style']): ?>
    </div>
    <?php endif; ?>
    <?php
}?>
<div class="rt-comment">
  
<div class="rt-header-block">
  <h2 class="rt-header-block__title">
    <?php _e('Leave a Reply', 'rt_domain')?>
  </h2>
</div>


<?php if (number_format_i18n( get_comments_number() ) > 0): ?>
<ul class="rt-comment-list">
<?php

wp_list_comments(array(
    'style'       => 'ul',
    'short_ping'  => true,
    'avatar_size' => 70,
    'callback'    => 'rt_comment_callback',
));
?>
</ul>


<div class="rt-pagination">
  <?php paginate_comments_links(array(
    'prev_text' => __('<span class="ti-arrow-left"></span>', 'rt_domain'),
    'next_text' => __('<span class="ti-arrow-right"></span>', 'rt_domain'),
  )); ?>
</div>


<?php endif;?>




<?php if (!comments_open() && get_comments_number() && post_type_supports(get_post_type(), 'comments')): ?>
  <p class="no-comments rt-alert rt-alert--info"><?php esc_html_e('Comments are closed.', 'rt_domain');?></p>
<?php endif;?>



 <?php
/** COMMENT FORM */
if (comments_open()):
    $comments_args = array(
        'title_reply'        => '',
        'label_submit'         => __('Send a Comment', 'rt_domain'),
        'id_form'              => 'commentform',
        'class_form'           => 'rt-comment-form js-comment',
        'comment_notes_before' => '<p class="comment_notes">' . __('Your email address will not be published.', 'rt_domain') . '</p>',
        'cancel_reply_before'  => '<small class="comment_cancel-reply">',
        'cancel_reply_after'   => '</small>',
    );

    comment_form($comments_args);

endif;
?>
</div>
