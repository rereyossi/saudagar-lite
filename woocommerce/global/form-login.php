<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.6.0
 */


 /**
 * Retheme 1.0.0 - Overridden
 * To do:
 * - add new DOM panel
 * - overridden all
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( is_user_logged_in() ) {
	return;
}

?>

<form class="rt-panel woocommerce-form woocommerce-form-login login" method="post" <?php echo ( $hidden ) ? 'style="display:none;"' : ''; ?>>
	
	
	<div class="rt-panel__header">
		<h4 class="rt-panel__title"><?php _e('Login', 'rt_domain')?></h4>
	</div>
	<div class="rt-panel__body">
		<?php do_action('woocommerce_login_form_start'); ?>

		<?php echo ($message) ? wpautop(wptexturize($message)) : ''; // @codingStandardsIgnoreLine ?>

		<div class="rt-form">
			<label class="rt-form__label" for="username"><?php esc_html_e( 'Username or email', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
			<input type="text" class="input-text rt-form__input" name="username" id="username" autocomplete="username" />
		</div>
		<div class="rt-form">
			<label class="rt-form__label" for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
			<input class="input-text rt-form__input" type="password" name="password" id="password" autocomplete="current-password" />
		</div>

		<?php do_action( 'woocommerce_login_form' ); ?>

		<p class="flex flex-middle flex-between">
			<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
				<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
			</label>
			<a class="lost_password link-text" href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><i class="fa fa-life-ring"></i> <?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
		</p>

		<div class="rt-form__group">
			<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
			<button type="submit" class="rt-btn rt-btn--primary rt-btn--block" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_html_e( 'Login', 'woocommerce' ); ?></button>
			<input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ) ?>" />
		</div>

		<?php do_action('woocommerce_login_form_end'); ?>
	</div>
	
</form>