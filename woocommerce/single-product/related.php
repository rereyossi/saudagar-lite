<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

  /**
 * Retheme 1.0.0 - Overridden
 * To do:
 * - added Slider DOM wrapper
 * - added header woocommerce
 */


if (! defined('ABSPATH')) {
    exit;
}

if ($related_products) : ?>

	<?php 
        echo \Retheme\HTML::before_slider(array(
            'id' => 'product-related-slider',
            'class' => ["related products rt-slider js-slider mb-30"],
            'items-lg' => rt_option('woocommerce_single_related_show', 4),
            'items-md' => rt_option('woocommerce_single_related_show_tablet', 2),
            'items-sm' => rt_option('woocommerce_single_related_show_mobile', 2),
            'nav' => false,
            'dots' =>false,
        ));
        ?>

		<?php echo \Retheme\HTML::header_block(array(
        "title" => __("You also like", 'rt_domain'),
        "nav" => true,
        )) ?>

		
        <div class="rt-product-related rt-slider__main owl-carousel">
			<?php foreach ($related_products as $related_product) : ?>

				<?php
                    $post_object = get_post($related_product->get_id());

                    setup_postdata($GLOBALS['post'] =& $post_object);

                    wc_get_template_part('content', 'product'); ?>

			<?php endforeach; ?>

        </div>

    <?php echo \Retheme\HTML::after_slider();
    
 endif;

wp_reset_postdata();
