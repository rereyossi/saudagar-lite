<?php
// include all core file
require get_template_directory() . '/core/core.php';

if (!function_exists('rt_freemius')) {
    // Create a helper function for easy SDK access.
    function rt_freemius()
    {
        global $rt_freemius;

        if (!isset($rt_freemius)) {
            // Include Freemius SDK.
            require_once dirname(__FILE__) . '/freemius/start.php';

            $rt_freemius = fs_dynamic_init(array(
                'id' => '3380',
                'slug' => 'saudagar',
                'type' => 'theme',
                'public_key' => 'pk_66d9e4283ec8eb1d2f596b49b3803',
                'is_premium' => false,
                'has_addons' => false,
                'has_paid_plans' => false,
                'is_org_compliant' => false,
                'menu' => array(
                    'slug' => 'theme-panel',
                    'first-path' => 'admin.php?page=theme-dashboard',
                    'contact' => false,
                    'support' => false,
                    'parent' => array(
                        'slug' => 'options-general.php',
                    ),
                ),
            ));
        }

        return $rt_freemius;
    }

    // Init Freemius.
    rt_freemius();
    // Signal that SDK was initiated.
    do_action('rt_freemius_loaded');
}
