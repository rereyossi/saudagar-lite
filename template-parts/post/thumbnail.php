<div class="rt-post__thumbnail rt-img rt-img--full">
  <?php
  if(rt_option('blog_options_image_size', 'featured_medium') !== 'none'){
    rt_the_post_thumbnail(rt_option('blog_options_image_size', 'featured_medium'));
  }
  ?>
</div>