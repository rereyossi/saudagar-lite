<div class="rt-single">
    <?php rt_single_category() ?>
    <?php rt_single_title() ?>
    <?php rt_single_meta() ?>
    <?php rt_single_share() ?>
    <?php rt_single_thumbnail()?>
    <?php rt_single_content() ?>
    <?php rt_single_tags()?>
    <?php rt_single_share() ?>
</div>

<?php rt_single_navigation() ?>
<?php rt_single_author_info() ?>
<?php rt_single_related() ?>
<?php rt_single_comment() ?>

