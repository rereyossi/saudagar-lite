<?php if(rt_option('single_element_author_info', true)):?>

<?php
$author_id = get_the_author_meta('ID');
$author = "user_{$author_id}";
$author_avatar     = get_avatar($author_id , 30);
$author_avatar_url = rt_get_field('avatar', $author);
$first_name        = get_the_author_meta('first_name');
$last_name         = get_the_author_meta('last_name');
$author_desc       = get_the_author_meta('description');
$author_instagram  = rt_get_field('instagram', $author);
$author_linkedin   = rt_get_field('linkedin', $author);
$author_fb         = rt_get_field('facebook', $author);
$author_tw         = rt_get_field('twitter', $author);
$author_job        = rt_get_field('job', $author);
$author_blog       = get_the_author_meta('blog_title');
$author_url        = get_the_author_meta('url');
$author_posts      = get_author_posts_url($author_id );
$author_website    = get_the_author_meta('url');
?>


<div class="rt-profile rt-profile--list mb-30">

  <div class="rt-profile__thumbnail rt-img rt-img--full">
    <?php
    if (!empty($author_avatar_url)) {
      echo wp_get_attachment_image($author_avatar_url, 'thumbnail');
    } else {
      echo $author_avatar;
    }
    ?>
  </div>


  <div class="rt-profile__body">

    <h4 class="rt-profile__title">
      <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')) ?>">
      <?php echo $first_name.' '. $last_name ?>
      </a>
    </h4>
    
    <span class="rt-profile__job"><?php echo $author_job ?></span>

    <p class="rt-profile__content">
        <?php echo $author_desc ?>
    </p>

    <div class="rt-profile__socmed rt-socmed rt-socmed--border">
      <?php if (!empty($author_instagram)): ?>
        <a href="<?php echo $author_instagram ?>" class="item rt-socmed__item instagram">
            <i class="fa fa-instagram" aria-hidden="true"></i>
        </a>
      <?php endif?>

      <?php if (!empty($author_fb)): ?>
        <a href="<?php echo $author_fb ?>" class="item rt-socmed__item facebook">
          <i class="fa fa-facebook-f" aria-hidden="true"></i>
        </a>
      <?php endif?>

      <?php if (!empty($author_tw)): ?>
        <a href="<?php echo $author_tw ?>" class="item rt-socmed__item twitter">
          <i class="fa fa-twitter" aria-hidden="true"></i>
        </a>
      <?php endif?>


      <?php if (!empty($author_youtube)): ?>
        <a href="<?php echo $author_youtube ?>" class="item rt-socmed__item youtube">
          <i class="fa fa-youtube" aria-hidden="true"></i>
        </a>
      <?php endif?>

      <?php if (!empty($author_linkedin)): ?>
        <a href="<?php echo $author_linkedin ?>" class="item rt-socmed__item linkedin">
          <i class="fa fa-linkedin" aria-hidden="true"></i>
        </a>
      <?php endif?>

    </div>

  </div>
</div>

<?php endif ?>