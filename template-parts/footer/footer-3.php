
<footer id="page-footer" class="page-footer page-footer--3">

    <?php if(rt_option('footer_widget', true)):?>

    <div class="page-footer__widget">
        <div class="page-container">
            
               <div class="flex flex-row flex-cols-md-3 flex-cols-sm-6 flex-cols-sm-12">
            
                    <div class="flex-item widget-footer-1">
                    <?php if(function_exists('dynamic_sidebar') && dynamic_sidebar('Footer Column 1')):else: ?>
                        <?php if ( is_active_sidebar( 'retheme_footer_1' ) ) :
                            dynamic_sidebar( 'retheme_footer_1' );
                        endif; ?>
                    <?php endif; ?>
                    </div>

            
                    <div class="flex-item widget-footer-2">
                    <?php if(function_exists('dynamic_sidebar') && dynamic_sidebar('Footer Column 2')):else: ?>
                        <?php if ( is_active_sidebar( 'retheme_footer_2' ) ) :
                            dynamic_sidebar( 'retheme_footer_2' );
                        endif; ?>
                    <?php endif; ?>
                    </div>

               </div>

        </div>

    </div>

    <?php endif; ?>

    <?php if(rt_option('footer_bottom', true)):?>
        <?php rt_get_template_part('footer/footer-bottom');?>
        
    <?php endif; ?>


</footer>
