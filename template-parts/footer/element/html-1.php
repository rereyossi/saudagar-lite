<?php if(rt_option('footer_bottom_text', true)): ?>
<span class="rt-footer__bottom-text"><?php echo rt_option('footer_bottom_text', '@Copyright '.rt_var('product-name').'. All Rights Reserved') ?></span>
<?php endif ?>
