<?php
$classes[] = 'rt-menu rt-menu--horizontal js-menu';
?>
<div  <?php rt_set_class('rt_second_menu_class', $classes) ?> 
data-animatein='<?php echo rt_option('header_main_submenu_animation_in', 'transition.fadeIn') ?>'
data-duration='<?php echo rt_option('header_main_submenu_animation_duration', '300') ?>'>
  <?php $args = array(
      'container'      => 'rt-',
      'menu_class'     => 'rt-menu__main',
      'theme_location' => 'second',
  );
  wp_nav_menu($args);?>
</div>
