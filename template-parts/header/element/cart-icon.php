

<?php if (rt_option('ajax_mini_cart', true)): ?>

<div  class="rt-header__element rt-header__cart">
  <i class="rt-header__cart-icon ti-shopping-cart js-cart-trigger"></i>
  <span class="rt-header__cart-count js-cart-total">0</span>
</div>

<?php else: ?>

<div class="rt-header__element rt-header__cart">
  <a href="<?php echo get_permalink(wc_get_page_id('cart')); ?>">
    <i class="rt-header__cart-icon ti-shopping-cart"></i>
    <span class="rt-header__cart-count js-cart-total">0</span>
  </a>
</div>

<?php endif ?>