<?php
$classes[] = 'rt-socmed rt-socmed--'.rt_option('header_social_style', 'simple');
 ?>
<?php if (rt_option('social_item') ): ?>
<div  class="rt-header__element header_social">
  <div <?php rt_set_class('rt_sosmed_header_class', $classes) ?>>

    <?php foreach (rt_option('social_item') as $key => $value): ?>

      <a href="<?php echo $value['link_url'] ?>" class="rt-socmed__item <?php echo $value['link_text'] ?>">
            <i class="fa fa-<?php echo $value['link_text'] ?>"></i>
      </a>
    <?php endforeach; ?>

  </div>
</div>
<?php endif; ?>
