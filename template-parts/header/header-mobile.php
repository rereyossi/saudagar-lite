<?php

$classes[] = 'rt-menu rt-menu--canvas js-mobile-menu';
$classes[] = !empty(rt_get_field('header_overlay')) ? 'is-overlay' : '';


/** Options */
$elements = rt_option_header();



if(rt_is_free()){
  $sticky = false;
}
?>

<div id="header-mobile" class="rt-header-mobile js-header-mobile"  data-sticky='<?php echo $sticky ?>'>

  <?php do_action('rt_before_header_mobile_main');?>

  <div class="rt-header-mobile__main">
    <div class="page-container">

       <div id="header-mobile-left" class="rt-header-mobile__column" data-alignment="<?php echo $elements['mobile_left_alignment'] ?>" data-display="<?php echo $elements['mobile_left_display'] ?>">
         <?php do_action('rt_header_mobile_left') ?>
      </div>
      
       <div id="header-mobile-center" class="rt-header-mobile__column" data-alignment="<?php echo $elements['mobile_center_alignment'] ?>" data-display="<?php echo $elements['mobile_center_display'] ?>">
           <?php do_action('rt_header_mobile_center')?>
      </div>

       <div id="header-mobile-right" class="rt-header-mobile__column" data-alignment="<?php echo $elements['mobile_right_alignment'] ?>" data-display="<?php echo $elements['mobile_right_display'] ?>">
           <?php do_action('rt_header_mobile_right')?>
      </div>

    </div>

  </div>

  <?php do_action('rt_after_header_mobile_main');?>

  <?php rt_get_template_part('header/header-search'); ?>

</div>
