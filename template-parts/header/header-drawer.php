<?php 
$classes[] = 'rt-main-canvas-menu rt-main-canvas-menu--dropdown js-main-menu-canvas'; 
$classes[] = 'rt-main-canvas-menu--'.rt_option('header_drawer_menu_schema', 'dark');
?>

<div <?php rt_set_class('header_mobile_drawer', $classes); ?>>
    <?php if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('mobile_drawer')): ?>
    <?php do_action('rt_mobile_drawer')?>
    <?php endif ?>

</div>
