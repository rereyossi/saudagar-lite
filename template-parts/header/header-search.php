<?php
$search_style = rt_option('search_options_style', 'dropdown');
$classes[] = "rt-search js-search rt-search--{$search_style}";

?>

<div <?php rt_set_class('rt_search_class', $classes)?>>

  <?php if ($search_style === 'modal'): ?>
    <span class="rt-search__icon rt-search__close js-search-close ti-close"></span>
    <div class="rt-search__overlay"></div>
  <?php endif?>

  <div class="page-container">

    <?php if ($search_style !== 'modal'): ?>
      <span class="rt-search__icon rt-search__close js-search-close ti-close"></span>
    <?php endif?>

    <form class="rt-search__inner" action="<?php echo home_url('/'); ?>" method="get" >

      <i class="rt-search__icon ti-search"></i>

      <input type="text"  name="s" value="<?php the_search_query();?>"  class="rt-search__input" placeholder="<?php echo rt_option('search_options_text', 'Type Something and enter') ?>">
      <button type="submit" class="rt-search__btn"><?php _e('Search', 'rt_domain') ?></button>

      <input type="hidden" name="post_type" value="<?php echo rt_option('search_options_result', 'post') ?>" />

    </form>

  </div>

</div>
