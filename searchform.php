<div <?php rt_set_class('rt_search_widget_class', ['rt-search'])?>>
    <form class="rt-search__inner" action="<?php echo home_url('/'); ?>" method="get" >
        <input class="rt-search__input" type="text" placeholder="<?php echo rt_option('search_options_text', 'Type Something and enter') ?>" name="s" id="s">
        <input type="hidden" name="post_type" value="post" />
        <button type="submit" class="rt-search__btn"><i class="ti-search"></i></button>
    </form>
</div>