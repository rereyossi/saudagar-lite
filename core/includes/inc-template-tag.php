<?php

/*=================================================
 *  LIMIT TITLE
/*=================================================
 * @since 1.0.0
 * @param  number for limit string $length
 * @return string title limit
 */
function rt_the_title($limit = '')
{

    if (empty($limit)) {
        $limit = '99';
    }
    $title = get_the_title($post->ID);
    if (strlen($title) > $limit) {
        $title = substr($title, 0, $limit) . '...';
    }

    echo $title;
}

/*=================================================
 *  LIMIT CONTENT
/*================================================= */
/**
 * @since 1.0.0
 * @param [length: number for limit string output]
 * @return [string]
 */

function rt_the_content($length = 18)
{
    global $post;

    // Check for custom excerpt
    if (has_excerpt($post->ID)) {
        $output = $post->post_excerpt;
    } else {
        // No custom excerpt
        // Check for more tag and return content if it exists
        if (strpos($post->post_content, '<!--more-->')) {
            $output = apply_filters('the_content', get_the_content());
        } else {
            // No more tag defined
            $output = wp_trim_words(strip_shortcodes($post->post_content), $length);
        }

    }

    echo $output;

}

/*=================================================;
/* PAGINATION
/*================================================= */
function rt_pagination()
{
    global $wp_query;
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $total_pages = $wp_query->max_num_pages;
    $big = 999999999; // need an unlikely integer

    $loop = apply_filters('rt_loop_query', array(
        'pagination' => 'number',
        'post_type' => 'post',
        'posts_per_page' => 7,
        'template_part' => 'template-parts/post/content-post',
    ));

    $settings = json_encode(array(
        'post_type' => $loop['post_type'],
        'posts_per_page' => $loop['posts_per_page'],
        'template_part' => $loop['template_part'],
    ));

    if ($loop['pagination'] == 'number' && $paged <= $total_pages) {

        echo '<div class="rt-pagination">' .
        paginate_links(array(
            'prev_text' => __('<span class="ti-arrow-left"></span>', 'rt_domain'),
            'next_text' => __('<span class="ti-arrow-right"></span>', 'rt_domain'),
        ))
            . '</div>';
    }

    if ($loop['pagination'] == 'loadmore' && $paged != $total_pages) {

        echo '<script>
	                var post_archive =' . $settings . ';
	              </script>';

        echo '<div class="rt-pagination rt-pagination--loadmore">
	                  <div class="rt-pagination__spinner js-post_archive-spinner">
	                      <i class="fa fa-spinner fa-spin fa-3x"></i>
	                  </div>
	                  <a href="#" data-triger-id="post_archive" class="rt-pagination__button js-loop-load rt-btn">' . __('Load More', 'rt_domain') . '</a>
	               </div>';
    }
}
add_action('rt_after_loop', 'rt_pagination', 8);

/*=================================================;
/* LOOP
/*================================================= */
function rt_loop_none()
{
    echo apply_filters('rt_loop_none', __('Post not found', 'rt_domain'));
}
add_action('rt_loop_none', 'rt_loop_none');

/*=================================================
 * IMAGE THUMBNAIL
/*================================================= */
function rt_the_post_thumbnail($size = '')
{
    $size = !empty($size) ? $size : 'featured_medium';

    if (has_post_thumbnail()) {
        the_post_thumbnail($size);
    } else {
        rt_image_placeholder($size);
    }
}



/*=================================================
 *  CONDITION TAGS
/*=================================================
 * @since 1.0.0
 * @desc cek plugin WooCommerce active
 * @param page WooCommerce page $page
 * @return boolean
 */

if (!function_exists('is_woocommerce_activated')) {
    function rt_is_woocommerce($page = '')
    {
        if (class_exists('WooCommerce')) {
            // Rertuns true on WooCommerce Plugins active
            if (empty($page)) {
                return true;
            }

            // all template WooCommerce but cart and checkout not include
            if ($page == 'pages' && is_woocommerce()) {
                return true;
            }

            // Returns true when on the product archive page (shop).
            if ($page == 'shop' && is_shop()) {
                return true;
            }

            if ($page == 'product' && is_product()) {
                return true;
            }

            // Returns true on the customer’s account pages.
            if ($page == 'account_page' && is_account_page()) {
                return true;
            }

            // Returns true on the cart page.
            if ($page == 'cart' && is_cart()) {
                return true;
            }

            // Returns true on the checkout page.
            if ($page == 'checkout' && is_checkout()) {
                return true;
            }

            // Returns true when viewing a WooCommerce endpoint
            if ($page == 'endpoint_url' && is_wc_endpoint_url()) {
                return true;
            }

            if ($page == 'category' && is_product_category()) {
                return true;
            }

            if ($page == 'tag' && is_product_tag()) {
                return true;
            }
        } else {
            return false;
        }
    }
}

/*=================================================
/* BREADCRUMBS
/*================================================= */
function rt_breadcrumb()
{
    if (function_exists('breadcrumb_trail')) {
        echo "<div class='rt-breadcrumbs'>";
        breadcrumb_trail();
        echo "</div>";
    }
}



/*=================================================
/* MAIN TEMPLATE
/*=================================================
 * @since 1.0.0
 * @desc  this function get global template. get template part form template-parts folder
 * @param url template part $content
 */
function rt_get_template($content)
{
    get_header();

    do_action('rt_before_wrapper');

    echo '<section id="page-wrapper" ' . rt_set_class_raw('rt_page_wrapper_class', ['page-wrapper']) . '>';

    echo '<div id="page-container" ' . rt_set_class_raw('rt_page_container_class', ['page-container', 'flex']) . '>';

    do_action('rt_before_content');

    if (!is_404()) {

        if (have_posts()):

            do_action('rt_before_loop');

            if ($content !== 'woocommerce'): // load file if not WooCommerce page

                while (have_posts()): the_post();

                    rt_get_template_part($content);

                endwhile;

            else: // load file if WooCommerce  page

                while (woocommerce_content()): the_post();

                    woocommerce_content();

                endwhile;

            endif;

            do_action('rt_after_loop');

        else:

            rt_get_template_part('page/content-none');

        endif;

    } else {
        rt_get_template_part('page/content-404');
    }

    do_action('rt_after_content');

    do_action('rt_sidebar');

    echo '</div>';

    echo '</section>';

    do_action('rt_after_wrapper');

    get_footer();
}
