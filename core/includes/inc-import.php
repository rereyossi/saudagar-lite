<?php 
/*=================================================;
/* SIGN HOMEPAGE AND DEFAULT MENU
/*================================================= */
function rt_after_import_setup()
{
    // Assign menus to their locations.
    set_theme_mod('nav_menu_locations', array(
        'main-menu' => $main_menu->term_id,
        'primary' => get_term_by('name', 'Main Menu', 'nav_menu'),
        'second' => get_term_by('name', 'Second Menu', 'nav_menu'),
        'thirty' => get_term_by('name', 'Thirty Menu', 'nav_menu'),
        'topbar' => get_term_by('name', 'Top Bar Menu', 'nav_menu'),
        'footer' => get_term_by('name', 'Footer Menu', 'nav_menu'),
    )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title('Home');
    $blog_page_id = get_page_by_title('Blog');

    update_option('show_on_front', 'page');
    update_option('page_on_front', $front_page_id->ID);
    update_option('page_for_posts', $blog_page_id->ID);

}
add_action('pt-ocdi/after_import', 'rt_after_import_setup');