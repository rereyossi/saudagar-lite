<?php
/**
 * retheme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package retheme
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function rt_setup()
{
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on retheme, use a find and replace
     * to change 'retheme' to the name of your theme in all the template files.
     */
    load_theme_textdomain('rt_domain', get_template_directory() . '/languages');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    //Turn on wide images
    add_theme_support('align-wide');

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in one location.
    $menu['primary'] = 'Menu Primary';
    $menu['footer'] = 'Menu Footer';


    register_nav_menus($menu);

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('post-thumbnails');
    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    /**
     * Add theme support selective refresh for customizer
     */
    add_theme_support('customize-selective-refresh-widgets');

    /** IMAGE SIZE */
    add_image_size('featured_medium', 400, 225, true);
    add_image_size('featured_medium_nocrop', 400, 999999);

    add_theme_support('title-tag');

    if (!isset($content_width)) {
        $content_width = 1170;
    }

}
add_action('after_setup_theme', 'rt_setup');

/*=================================================;
/* BODY CLASS
/*================================================= */
function rt_theme_body_class($classes)
{


    if (rt_var('gutenberg-support')) {
        $classes[] = 'gutenberg-support';
    }

    return $classes;
}

add_filter('body_class', 'rt_theme_body_class');

/*=================================================;
/* SINGLE - REMOVE HEADER
/*================================================= */
function rt_single_remove_page_header($args)
{
    if (is_singular('post')) {
        return false;
    }
    return $args;

}
add_filter('page_header', 'rt_single_remove_page_header');

/*=================================================;
/* REQUIRED PLUGIN
/*================================================= */
/**
 * Register the required plugins for this theme.
 */
function rt_theme_required_plugins()
{
     /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $repo_plugins = array(
        // This is an example of how to include a plugin from the WordPress Plugin Repository.
        array(
            'name' => 'Elementor',
            'slug' => 'elementor',
        ),

        array(
            'name' => 'One Click Demo Import',
            'slug' => 'one-click-demo-import',
        ),

        array(
            'name' => 'WooCommerce',
            'slug' => 'woocommerce',
        ),
        array(
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
        ),
        array(
            'name' => 'Font Awesome',
            'slug' => 'font-awesome',
        ),

        array(
            'name' => 'Ongkos Kirim',
            'slug' => 'ongkoskirim-id',
        ),

    );


    $premium_plugins = array();

    $plugins = wp_parse_args($premium_plugins, $repo_plugins);

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id' => 'tgmpa', // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '', // Default absolute path to bundled plugins.
        'menu' => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug' => 'theme-panel', // Parent menu slug.
        'capability' => 'edit_theme_options', // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices' => true, // Show admin notices or not.
        'dismissable' => true, // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => true, // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false, // Automatically activate plugins after installation or not.
        'message' => '', // Message to output right before the plugins table.
    );

    tgmpa($plugins, $config);

}

add_action('tgmpa_register', 'rt_theme_required_plugins');

/*=================================================;
/* DEMO IMPORT
/*================================================= */
function rt_theme_demo_import()
{
    return array(

        array(
            'import_file_name' => 'Saudagar',
            'categories' => array('WooCommerce'),
            'import_file_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/saudagar-demo-default.xml',
            'import_widget_file_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/saudagar-demo-default.wie',
            'import_customizer_file_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/saudagar-demo-default.dat',
            'import_preview_image_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/saudagar-demo-default.png',
            'preview_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/saudagar-demo-default.png',
        ),
        array(
            'import_file_name' => 'Comming Soon',
            'categories' => array('WooCommerce'),
            'import_file_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/coming-soon.xml',
            'import_widget_file_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/coming-soon.wie',
            'import_customizer_file_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/coming-soon.dat',
            'import_preview_image_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/coming-soon.png',
            'preview_url' => 'https://webforia.id/wp-content/uploads/webforia/saudagar/coming-soon.png',
        ),

    );

}
add_filter('pt-ocdi/import_files', 'rt_theme_demo_import');

/*=================================================;
/* DEFAUT VARIABLE
/*================================================= */
function rt_var($value, $arg = '')
{
    $default = apply_filters('rt_var', array(
        'product-name' => 'Saudagar',
        'product-slug' => 'saudagar-theme',
        'product-version' => '1.2.2',
        'product-docs' => 'https://www.panduan.webforia.id/saudagar',
        'product-url' => 'https://www.webforia.id/saudagar',
        'product-group' => 'https://web.facebook.com/groups/265676900996871',
        'product-support' => 'https://web.facebook.com/groups/265676900996871',
        'product-contact' => 'https://webforia.id/kontak/',
        'font-primary' => 'Roboto',
        'font-weight' => '500',
        'font-second' => 'Roboto',
        'color-primary' => '#222222',
        'color-second' => '#333333',
        'font-size-body' => '14px',
        'line-height-body' => '1.5',
        'search-style' => 'dropdown',
        'header-default' => array(
            'topbar_left_element' => ['topbar-menu'],
            'topbar_left_display' => 'grow',
            'topbar_left_alignment' => 'left',
            'topbar_center_element' => [],
            'topbar_center_display' => 'normal',
            'topbar_center_alignment' => 'center',
            'topbar_right_element' => [],
            'topbar_right_display' => 'grow',
            'topbar_right_alignment' => 'right',

            'middle_left_element' => [],
            'middle_left_display' => 'grow',
            'middle_left_alignment' => 'left',
            'middle_center_element' => [],
            'middle_center_display' => 'normal',
            'middle_center_alignment' => 'center',
            'middle_right_element' => [],
            'middle_right_display' => 'grow',
            'middle_right_alignment' => 'right',

            'main_left_element' => ['logo'],
            'main_left_display' => 'grow',
            'main_left_alignment' => 'left',
            'main_center_element' => [],
            'main_center_display' => 'normal',
            'main_center_alignment' => 'center',
            'main_right_element' => ['main-menu', 'search-icon'],
            'main_right_display' => 'grow',
            'main_right_alignment' => 'right',

            'sticky_left_element' => ['logo'],
            'sticky_left_display' => 'grow',
            'sticky_left_alignment' => 'left',
            'sticky_center_element' => [],
            'sticky_center_display' => 'normal',
            'sticky_center_alignment' => 'center',
            'sticky_right_element' => ['main-menu', 'search-icon'],
            'sticky_right_display' => 'grow',
            'sticky_right_alignment' => 'right',

            'mobile_left_element' => ['logo-mobile'],
            'mobile_left_display' => 'grow',
            'mobile_left_alignment' => 'left',
            'mobile_center_element' => [],
            'mobile_center_display' => 'normal',
            'mobile_center_alignment' => 'center',
            'mobile_right_element' => ['toggle-menu'],
            'mobile_right_display' => 'grow',
            'mobile_right_alignment' => 'right',

            'drawer_element' => ['main-menu'],
        ),
        'extend' => array('astro-element', 'webforia-shop-booster', 'webforia-whatsapp-chat'),
    )
    );

    if ($arg) {
        $var = !empty($default[$value]) ? $default[$value] . $arg : '';
    } else {
        $var = !empty($default[$value]) ? $default[$value] : '';
    }

    return $var;

}
