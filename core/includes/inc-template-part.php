<?php
/*=================================================
 *  LAYOUT CLASS
/*================================================= */
/**
 * Add class in body each page for layout
 */
function rt_layout($classes)
{
    $classes[] = 'template template--' . rt_option('layout_setting_style', 'full');

    if (is_page_template('templates/template-full.php')) {
        $classes[] = 'template--builder-container';
    }

    return $classes;
}
add_filter('body_class', 'rt_layout', 5);

/*=================================================
 *  LOGO
/*================================================= */

function rt_logo($type = '')
{
    $header_overlay = rt_get_field('header_overlay');
    $logo_image = '';

    $logo_desktop_primary = !empty($header_overlay) ? rt_option('brand_logo_overlay') : rt_option('brand_logo_primary');
    $logo_desktop_sticky = !empty(rt_option('brand_logo_sticky')) ? rt_option('brand_logo_sticky') : rt_option('brand_logo_primary');

    $logo_mobile_primary = !empty($header_overlay) ? rt_option('brand_logo_overlay') : rt_option('brand_logo_mobile');
    $logo_mobile_sticky = !empty(rt_option('brand_logo_mobile_sticky')) ? rt_option('brand_logo_mobile_sticky') : rt_option('brand_logo_mobile');

    /* sticky logo desktop */
    if ($type != 'mobile') {
        $logo_primary = apply_filters('rt_logo_desktop_primary', $logo_desktop_primary);
        $logo_sticky = apply_filters('rt_logo_desktop_sticky', $logo_desktop_sticky);
    } else {
        /* logo mobile */
        $logo_primary = apply_filters('rt_logo_mobile_primary', $logo_mobile_primary);
        $logo_sticky = apply_filters('rt_logo_mobile_sticky', $logo_mobile_sticky);
    }

    /* Print logo */
    $output = '<a class="rt-logo" href="' . esc_url(home_url()) . '">';

    if ($logo_primary) {
        if ($type == 'primary') {
            $output .= '<img class="rt-logo__primary" src="' . $logo_primary . '" alt="' . get_bloginfo('name') . '">';
        } else {
            $output .= '<img class="rt-logo__sticky" src="' . $logo_sticky . '" alt="' . get_bloginfo('name') . '">';
            $output .= '<img class="rt-logo__primary" src="' . $logo_primary . '" alt="' . get_bloginfo('name') . '">';
        }
    } else {
        $output .= rt_option('blogname', 'Retheme');
    }

    $output .= '</a>';

    do_action('rt_before_brand');

    echo $output;

    do_action('rt_after_brand');
}
add_action('rt_logo', 'rt_logo');

/*=================================================
 *  HEADER
/*================================================= */
function rt_header()
{
    if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('header')) {
        rt_get_template_part('header/header-layout');
    }

}
add_action('rt_header', 'rt_header', 5);

/*=================================================
 *  PAGE PAGE TITLE
/*================================================= */
function rt_page_header()
{
    if (rt_option('page_header', true) == true && is_page_template('templates/template-full.php') != true) {
        rt_get_template_part('global/page-header');
    }
}
add_action('rt_header', 'rt_page_header', 10);

/*=================================================
 *  CONTENT
/*================================================= */
function rt_before_content()
{
    rt_get_template_part('global/content-start');
}
add_action('rt_before_content', 'rt_before_content', 5);

function rt_after_content()
{
    rt_get_template_part('global/content-end');
}
add_action('rt_after_content', 'rt_after_content', 5);

/*=================================================
 *  LOOP
/*================================================= */
function rt_before_loop()
{
    if (is_archive() || is_home() || is_search()) {
        rt_get_template_part('global/loop-start');
    }
}
add_action('rt_before_loop', 'rt_before_loop', 5);

function rt_after_loop()
{
    if (is_archive() || is_home() || is_search()) {
        rt_get_template_part('global/loop-end');
    }
}
add_action('rt_after_loop', 'rt_after_loop', 5);

/*=================================================
 *  SIDEBAR
/*================================================= */
function rt_get_sidebar($classes)
{

    $sidebar = 'right';
    $sidebar = rt_get_field('sidebar_layout');

    if (is_home()) {
        $sidebar = rt_option('sidebar_layout_home', 'right');
    }

    if (is_archive()) {
        $sidebar = rt_option('sidebar_layout_archive', 'right');
    }

    /** check sidebar meta, if page or post set default sidebar
     * use customizer option if no sidebar use meta setting
     *  @ rt_get_field('sidebar_layout') get sidebar from meta setting
     * */

    if (is_single()) {

        if (!empty(rt_get_field('sidebar_layout')) && rt_get_field('sidebar_layout') != 'default') {
            $sidebar = rt_get_field('sidebar_layout');
        } else {
            $sidebar = rt_option('sidebar_layout_single', 'right');
        }
    }
    if (is_page()) {

        if (!empty(rt_get_field('sidebar_layout')) && rt_get_field('sidebar_layout') != 'default') {
            $sidebar = rt_get_field('sidebar_layout');
        } else {
            $sidebar = rt_option('sidebar_layout_page', 'right');
        }
    }

    // WooCommerce
    if (rt_is_woocommerce('pages')
        || rt_is_woocommerce('cart')
        || rt_is_woocommerce('checkout')
        || rt_is_woocommerce('account_page')) {
        $sidebar = 'none';
    }

    if (rt_is_woocommerce('shop') || is_tax(array('product_cat', 'product_tag'))) {
        $sidebar = rt_option('sidebar_layout_woocommerce_archive', 'right');
    }

    if (rt_is_woocommerce('product')) {
        $sidebar = rt_option('sidebar_layout_woocommerce_single', 'none');
    }

    // Tempate full
    if (is_page_template('templates/template-full.php')) {
        $sidebar = 'none';
    }

    // gutenberg support
    if(rt_var('gutenberg-support')){
     $sidebar = 'none';
    }

    // set sidebar filter
    $sidebar = apply_filters('rt_sidebar_layout', $sidebar);

    if ($classes === 'status') {
        $classes = $sidebar;
    } else {
        $classes[] = 'template--sidebar-' . $sidebar;
    }

    return $classes;

}

add_filter('body_class', 'rt_get_sidebar');

/*=================================================
 *  SIDEBAR STATUS
/*================================================= */
function rt_sidebar()
{
    if (rt_get_sidebar('status') != 'none') {
        get_sidebar();
    }

}
add_action('rt_sidebar', 'rt_sidebar', 5);

/*=================================================;
/* SIDEBAR WIDGETS
/*================================================= */
function rt_sidebar_widget()
{
    /** Check Elementor Pro Location */
    if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('sidebar')) {
        if (rt_is_woocommerce('pages')) {
            dynamic_sidebar('retheme_woocommerce_sidebar');
        } else {
            dynamic_sidebar('retheme_sidebar');
        }
    }
}
add_action('rt_sidebar_widget', 'rt_sidebar_widget', 5);

/*=================================================;
/* SIDEBAR STICKY
/*================================================= */
function rt_sidebar_sticky($args){
    if(rt_is_free()){
        return false;
    }

    return $args;
}
add_filter('sidebar_option_sticky', 'rt_sidebar_sticky');

/*=================================================
 *  FOOTER
/*================================================= */
function rt_footer()
{
    if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('footer')) {
        rt_get_template_part('footer/' . rt_option('footer_widget_layout', 'footer-1'));
    }

}
add_action('rt_footer', 'rt_footer', 5);

/*=================================================
 * GLOBAL TOP
=================================================== */
function rt_gotop()
{
    rt_get_template_part('global/gotop');
}
add_action('rt_footer', 'rt_gotop', 10);

/*=================================================;
/* MENU DESKTOP MAIN
/*================================================= */
function rt_header_main_left()
{
    $options = rt_option('header_builder_option');
    $defaults = array('main_left_element' => ['logo']);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['main_left_element'])) {
        foreach ($elements['main_left_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_main_left', 'rt_header_main_left', 5);

function rt_header_main_center()
{
    $options = rt_option('header_builder_option');
    $defaults = array('main_center_element' => []);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['main_center_element'])) {
        foreach ($elements['main_center_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_main_center', 'rt_header_main_center', 5);

function rt_header_main_right()
{
    $options = rt_option('header_builder_option');
    $defaults = array('main_right_element' => ['main-menu', 'search-icon']);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['main_right_element'])) {
        foreach ($elements['main_right_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_main_right', 'rt_header_main_right', 5);

/*=================================================;
/* MENU DESKTOP STICKY
/*================================================= */
function rt_header_sticky_left()
{
    $options = rt_option('header_builder_option');
    $defaults = array('sticky_left_element' => ['logo']);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['sticky_left_element'])) {
        foreach ($elements['sticky_left_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_sticky_left', 'rt_header_sticky_left', 5);


function rt_header_sticky_center()
{
    $options = rt_option('header_builder_option');
    $defaults = array('sticky_center_element' => []);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['sticky_center_element'])) {
        foreach ($elements['sticky_center_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_sticky_center', 'rt_header_sticky_center', 5);

function rt_header_sticky_right()
{
    $options = rt_option('header_builder_option');
    $defaults = array('sticky_right_element' => ['main-menu', 'search-icon']);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['sticky_right_element'])) {
        foreach ($elements['sticky_right_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_sticky_right', 'rt_header_sticky_right', 5);

/*=================================================;
/* HEADER DESKTOP MIDDLE
/*================================================= */

function rt_header_middle_left()
{
    $options = rt_option('header_builder_option');
    $defaults = array('middle_left_element' => []);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['middle_left_element'])) {
        foreach ($elements['middle_left_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_middle_left', 'rt_header_middle_left', 5);

function rt_header_middle_center()
{
    $options = rt_option('header_builder_option');
    $defaults = array('middle_center_element' => []);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['middle_center_element'])) {
        foreach ($elements['middle_center_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_middle_center', 'rt_header_middle_center', 5);

function rt_header_middle_right()
{
    $options = rt_option('header_builder_option');
    $defaults = array('middle_right_element' => []);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['middle_right_element'])) {
        foreach ($elements['middle_right_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_middle_right', 'rt_header_middle_right', 5);

/*=================================================;
/* HEADER DESKTOP TOPBAR
/*================================================= */
function rt_header_topbar_left()
{
    $options = rt_option('header_builder_option');
    $defaults = array('topbar_left_element' => ['topbar-menu']);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['topbar_left_element'])) {
        foreach ($elements['topbar_left_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_topbar_left', 'rt_header_topbar_left', 5);

function rt_header_topbar_center()
{
    $options = rt_option('header_builder_option');
    $defaults = array('topbar_center_element' => []);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['topbar_center_element'])) {
        foreach ($elements['topbar_center_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_topbar_center', 'rt_header_topbar_center', 5);

function rt_header_topbar_right()
{
    $options = rt_option('header_builder_option');
    $defaults = array('topbar_right_element' => []);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['topbar_right_element'])) {
        foreach ($elements['topbar_right_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_topbar_right', 'rt_header_topbar_right', 5);

/*=================================================;
/* HEADER MOBILE
/*================================================= */
function rt_header_mobile_left()
{
    $options = rt_option('header_builder_option');
    $defaults = array('mobile_left_element' => ['logo-mobile']);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['mobile_left_element'])) {
        foreach ($elements['mobile_left_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_mobile_left', 'rt_header_mobile_left', 5);

function rt_header_mobile_center()
{
    $options = rt_option('header_builder_option');
    $defaults = array('mobile_center_element' => []);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['mobile_center_element'])) {
        foreach ($elements['mobile_center_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_mobile_center', 'rt_header_mobile_center', 5);

function rt_header_mobile_right()
{
    $options = rt_option('header_builder_option');
    $defaults = array('mobile_right_element' => ['toggle-menu']);
    $elements = wp_parse_args($options, $defaults);

    if (!empty($elements['mobile_right_element'])) {
        foreach ($elements['mobile_right_element'] as $element) {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_header_mobile_right', 'rt_header_mobile_right', 5);

/*=================================================;
/* MOBILE DRAWER
/*================================================= */
// add drawer location on mobile header
function rt_add_mobile_drawer()
{
    if (rt_option('header_drawer_menu_style', 'dropdown') === 'dropdown') {
        rt_get_template_part('header/header-drawer');
    }
}
add_action('rt_after_header_mobile_main', 'rt_add_mobile_drawer', 5);

// mobile drawer panel
function rt_add_mobile_drawer_panel()
{
    if (rt_option('header_drawer_menu_style', 'dropdown') === 'sidepanel') {
        rt_get_template_part('header/header-drawer-panel');
    }

}
add_action('rt_footer', 'rt_add_mobile_drawer_panel');

// add mobile drawer content
function rt_mobile_drawer()
{

    $options = rt_option('header_builder_option');
    $defaults = array('drawer_element' => ['main-menu']);

    $elements = wp_parse_args($options, $defaults);

    foreach ($elements['drawer_element'] as $element) {
        if ($element === 'main-menu') {
            rt_get_template_part('header/element/mobile-menu');
        } else {
            rt_get_template_part('header/element/' . $element);
        }
    }

}
add_action('rt_mobile_drawer', 'rt_mobile_drawer', 5);
