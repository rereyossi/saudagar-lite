<?php

/*=================================================;
/* TEMPLATE PART
/*================================================= */
function rt_get_template_part($template, $data = '')
{
    $loader = new Retheme\Template_Loader;

    $loader->set_template_data($data)
        ->get_template_part("template-parts/{$template}");

}

/*=================================================
 *  LIMIT STRING
/*================================================= */
function rt_limited_string($the_excerpt, $excerpt_length)
{
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt));
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if (count($words) > $excerpt_length):
        array_pop($words);
        $the_excerpt = implode(' ', $words);
        $the_excerpt .= '...';
    endif;

    return $the_excerpt;
}

/*=================================================;
/* SET CLASS ROW
/*================================================= */
function rt_set_class_raw($filter = '', $classes = array())
{
    if (!empty($filter)) {
        $class_output = join(' ', apply_filters($filter, array_unique($classes)));
    } else {
        $class_output = join(' ', array_unique($classes));
    }

    return 'class="' . $class_output . '"';
}

/*=================================================
 *  SET CLASS
/*================================================= */
function rt_set_class($filter = '', $classes = array())
{
    echo rt_set_class_raw($filter, $classes);
}
