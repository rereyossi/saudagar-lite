<?php

/*=================================================;
/* REGISTER WIDGET LOCATION
/*================================================= */
function rt_register_widget_location()
{

    register_sidebar(array(
        'name' => __('Sidebar', 'rt_domain'),
        'id' => 'retheme_sidebar',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--aside %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
        'after_title' => '</h3></div>',
    ));

    if (class_exists('woocommerce')) {

        register_sidebar(array(
            'name' => __('WooCommerce Sidebar', 'rt_domain'),
            'id' => 'retheme_woocommerce_sidebar',
            'before_widget' => '<div id="%1$s" class="rt-widget rt-widget--aside %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
            'after_title' => '</h3></div>',
        ));
    }

    register_sidebar(array(
        'name' => __('Footer Column 1', 'rt_domain'),
        'id' => 'retheme_footer_1',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
        'after_title' => '</h3></div>',
    ));

    register_sidebar(array(
        'name' => __('Footer Column 2', 'rt_domain'),
        'id' => 'retheme_footer_2',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
        'after_title' => '</h3></div>',
    ));

    register_sidebar(array(
        'name' => __('Footer Column 3', 'rt_domain'),
        'id' => 'retheme_footer_3',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
        'after_title' => '</h3></div>',
    ));

    register_sidebar(array(
        'name' => __('Footer Column 4', 'rt_domain'),
        'id' => 'retheme_footer_4',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
        'after_title' => '</h3></div>',
    ));

    register_sidebar(array(
        'name' => __('Footer Column 5', 'rt_domain'),
        'id' => 'retheme_footer_5',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
        'after_title' => '</h3></div>',
    ));

    register_sidebar(array(
        'name' => __('Footer Column 6', 'rt_domain'),
        'id' => 'retheme_footer_6',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
        'after_title' => '</h3></div>',
    ));

}
 add_action('widgets_init', 'rt_register_widget_location');
 
/*=================================================;
/* SET SIZE TAG
/*================================================= */
function rt_tag_cloud_widget($args)
{
    $args['largest'] = 12;
    $args['smallest'] = 12;
    $args['unit'] = 'px';

    return $args;
}
add_filter('widget_tag_cloud_args', 'rt_tag_cloud_widget');