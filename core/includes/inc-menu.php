<?php
/*=================================================;
/* MENU
/*================================================= */
/**
 * Add css class to menu item
 * @category Menu
 * @param [type] $classes
 * @param [type] $item
 * @return void
 */
function rt_menu_class($classes, $item)
{
    if ($item->current) {
        $classes[] = "is-active";
    }

    $classes[] = "rt-menu__item";

    return $classes;

}
add_filter('nav_menu_css_class', 'rt_menu_class', 10, 2);

/**
 * add class sub menu
 * @category menu
 * @param [type] $menu
 * @return void
 */
function rt_submenu_class($menu)
{
    $menu = preg_replace('/ class="sub-menu"/', '/ class="sub-menu rt-menu__submenu" /', $menu);
    return $menu;
}

add_filter('wp_nav_menu', 'rt_submenu_class');

/*=================================================;
/* MENU TERMS
/*================================================= */
/**
 * add parent child term  to menu
 *
 * @param [type] $taxonomy
 * @param string $style
 * @return void
 */
function rt_menu_terms($taxonomy, $style = '')
{
    $style = !empty($style) ? $style : 'rt-menu--horizontal';

    $args = array('parent' => 0);

    // get all direct decendants of the $parent
    $terms = get_terms($taxonomy, $args);

    if (count($terms)):

        echo '<div id="menu-' . $taxonomy . '" class="rt-menu ' . $style . ' js-menu">';
        echo '<ul class="rt-menu__main">';

        foreach ($terms as $term) {

            $children = get_term_children($term->term_id, $taxonomy);

            // get parent term
            echo '<li class="rt-menu__item"><a href="' . get_term_link($term->slug, $taxonomy) . '" class="' . $term->slug . '">' . $term->name . '</a>';

            // get child term
            if (count($children)) {
                echo '<ul class="rt-menu__submenu">';
                foreach ($children as $child) {
                    $child = get_term_by('id', $child, $taxonomy);
                    echo '<li class="rt-menu__item"><a href="' . get_term_link($child, $taxonomy) . '" class="' . $child->slug . '">' . $child->name . '</a></li>';
                }
                echo '</ul>';
            }

            echo '</li>'; // end parent menu

        }

        echo '</ul>';
        echo '</div>';

    endif;

}


/*=================================================;
/* SET DEFAULT MENU
/*================================================= */

// Check if the menu exists
$menu_name = 'Main Menu';
$menu_exists = wp_get_nav_menu_object( $menu_name );

// If it doesn't exist, let's create it.
if( empty($menu_exists->slug)){
    $menu_id = wp_create_nav_menu($menu_name);
}

$locations = get_theme_mod('nav_menu_locations');
$location['primary'] = 'Main Menu';
set_theme_mod('nav_menu_locations', $location);

