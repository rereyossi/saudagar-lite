<?php

/*=================================================;
/* REGISTER STYLE
/*================================================= */
function rt_style()
{
    /* Vendor */
    wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', '2.3.4');

    /* Font */
    wp_enqueue_style('themify-icon', get_template_directory_uri() . '/assets/themify-icons/css/themify-icons.min.css', '1.0.0');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', '4.7.0');

    /* Retheme Style */
    if (rt_is_woocommerce()) {
        wp_enqueue_style('retheme-woocommerce', get_template_directory_uri() . '/assets/css/retheme-woo.min.css', '1.0.0');
    }
    wp_enqueue_style('retheme', get_template_directory_uri() . '/assets/css/retheme.min.css', '1.2.0');

}

add_action('wp_enqueue_scripts', 'rt_style');

/*=================================================;
/* REGISTER SCRIPTS
/*================================================= */
function rt_scripts()
{
    /* Vendor */
    wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), '2.3.4', true);
    wp_enqueue_script('velocity', get_template_directory_uri() . '/assets/js/velocity.min.js', false, '1.5.0', true);
    wp_enqueue_script('velocity-ui', get_template_directory_uri() . '/assets/js/velocity.ui.min.js', false, '5.2.0', true);

    if (rt_option('sidebar_sticky', true)) {
        wp_enqueue_script('sticky-kit', get_template_directory_uri() . '/assets/js/jquery.sticky-kit.min.js', array('jquery'), '1.1.2', true);

    }


    if (class_exists('woocommerce')) {
        wp_enqueue_script('retheme-woocommerce', get_template_directory_uri() . '/assets/js/woocommerce.min.js', array('jquery'), '1.0.0', true);
    }

    wp_enqueue_script('retheme', get_template_directory_uri() . '/assets/js/main.min.js', array('jquery'), '1.0.0', true);

    /* Comment */
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

}

add_action('wp_enqueue_scripts', 'rt_scripts');

/*=================================================;
/* GET ALL SCRIPTS
/*================================================= */
function rt_get_scipts()
{
    rt_style();
    rt_scripts();
}
