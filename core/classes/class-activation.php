<?php
namespace Retheme;

class Activation
{

    public $secret_key = '5c0a2dd7949698.97710041s';
    public $server_host = 'https://webforia.id/';

    public function __construct()
    {
        add_action('admin_notices', array($this, 'check_license'));
        add_action('after_switch_theme', array($this, 'after_theme_setup'));

    }

    public function after_theme_setup(){
         $this->deactivation(get_option(rt_var('product-slug', '_key')));
    }

    public function check_license()
    {
        if (!empty($_POST['theme_license_submit'])) {

            if ($_POST['theme_license_submit'] == 'Activate License') {
                $this->activation($_POST['theme_license_key']);
            } else {
                $this->deactivation($_POST['theme_license_key']);
            }

        }

    }

    /**
     * check product regiter for this license key
     *
     * @param [type] $key
     * @return string
     */
    public function get_product($key)
    {

        $api_params = array(
            'slm_action' => 'slm_check',
            'secret_key' => $this->secret_key,
            'license_key' => $key,
        );
        // Send query to the license manager server
        $response = wp_remote_get(add_query_arg($api_params, $this->server_host), array('timeout' => 20, 'sslverify' => false));

        $license_data = json_decode(wp_remote_retrieve_body($response));

        return $license_data->product_ref;

    }

    public function activation($key)
    {

        $api_params = array(
            'slm_action' => 'slm_activate',
            'secret_key' => $this->secret_key,
            'license_key' => $key,
            'registered_domain' => str_replace(array('www.', 'http://', 'https://'), '', site_url()),
        );

        // Send query to the license manager server
        $query = esc_url_raw(add_query_arg($api_params, $this->server_host));
        $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));

        // Check for error in the response
        if (is_wp_error($response)) {
            Notice::get_error_notice('Proses aktivasi bermasalah, pastikan Anda terhubung dengan internet');

            return false;
        }

        // License data.
        $license_data = json_decode(wp_remote_retrieve_body($response));

        if ($license_data->result == 'success') { //Success was returned for the license activation

            // check product refrensi
            if ($this->get_product($key) == rt_var('product-slug')) {
                update_option(rt_var('product-slug', '_status'), 'active');
                update_option(rt_var('product-slug', '_key'), $key);

                // extend plugin include active
                foreach (rt_var('extend') as $key => $plugin) {
                    update_option("{$plugin}_status", 'active');
                    update_option("{$plugin}_key", $key);
                }


                Notice::get_success_notice($license_data->message);
                
            } else {
                Notice::get_error_notice('Product not register for this license key');
            }

        } else {
            //Show error to the user. Probably entered incorrect license key.

            update_option(rt_var('product-slug', '_status'), 'deactive');
            update_option(rt_var('product-slug', '_key'), '');

            // extend plugin include deactive
            foreach (rt_var('extend') as $key => $plugin) {
                update_option("{$plugin}_status", 'deactive');
                update_option("{$plugin}_key", '');
            }
    

            Notice::get_error_notice($license_data->message);
        }

    }

    public function deactivation($key)
    {

        // API query parameters
        $api_params = array(
            'slm_action' => 'slm_deactivate',
            'secret_key' => $this->secret_key,
            'license_key' => $key,
            'registered_domain' => str_replace(array('www.', 'http://', 'https://'), '', site_url()),
        );

        // Send query to the license manager server
        $query = esc_url_raw(add_query_arg($api_params, $this->server_host));
        $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));

        // Check for error in the response
        if (is_wp_error($response)) {
            echo "Unexpected Error! The query returned with an error.";
        }

        // License data.
        $license_data = json_decode(wp_remote_retrieve_body($response));

        if ($license_data->result == 'success') { //Success was returned for the license activation
            update_option(rt_var('product-slug', '_status'), 'deactive');
            update_option(rt_var('product-slug', '_key'), '');

            // extend plugin include deactive
            foreach (rt_var('extend') as $key => $plugin) {
                update_option("{$plugin}_status", 'deactive');
                update_option("{$plugin}_key", '');
            }
            
            //Uncomment the followng line to see the message that returned from the license server
            Notice::get_success_notice($license_data->message);

        } else {
            //Show error to the user. Probably entered incorrect license key.
            Notice::get_error_notice($license_data->message);

        }

    }

    /**
     * Check status on option
     */

    public function get_status()
    {

        return get_option(rt_var('product-slug', '_status'));

    }

    public static function active()
    {
        return (get_option(rt_var('product-slug', '_status')) === 'active') ? true : false;
    }

}
new Activation;
