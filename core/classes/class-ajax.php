<?php
namespace Retheme;

use Retheme\Helper;
use Retheme\HTML;

 class Class_Ajax
    {
        public function __construct()
        {
            add_action('wp_ajax_ajax_loop_result', [$this, 'loop_result']);
            add_action('wp_ajax_nopriv_ajax_loop_result', [$this, 'loop_result']);

            add_action('wp_enqueue_scripts', [$this, 'load_scripts']);

        }

        /**
         * return query
         *
         * @return [post]
         */
        public function loop_result()
        {
 
            // get setting form json
            $settings = $_POST['settings'];
            
   
            // get post archive or block form elementor
            $block = $_POST['block'];

            $query_standard = json_decode(stripslashes($_POST['query']), true);

            // if null
            $query_by = array();

            $query_by = array(
                'post_type' => $settings['post_type'],
                'paged' => $_POST['page'],
                'query_by' => $settings['query_by'],
                'orderby' => $settings['orderby'],
                'order' => $settings['order'],
                'posts_per_page' => $settings['posts_per_page'],
                'post_status' => 'publish',
            );

            // Merge Array
            if ($block == 'post_archive') {
                $args = wp_parse_args(Helper::query($query_by), $query_standard);
            } else {
                $args = Helper::query($query_by);
            }


            $the_query = new \WP_Query($query_by);

            if ($the_query->have_posts()) {
                while ($the_query->have_posts()): $the_query->the_post();

                    include locate_template($settings['template_part'] . '.php');

                endwhile;

                wp_reset_postdata(); 

            } else {
                rt_post_none();
            }


            die();
        }

        /**
         * load scripts
         *
         * @return [inject ajax-loop.js, js variable]
         */
        public function load_scripts()
        {
             global $wp_query;

            $max = $wp_query->max_num_pages;
            $paged = get_query_var('paged') ? get_query_var('paged') : 1;

            wp_enqueue_script('ajax_loop', get_template_directory_uri() . '/core/classes/assets/js/ajax-loop.min.js', array('jquery'), '1.0.0', true);

            wp_localize_script(
                'ajax_loop',
                'ajax_loop',
                array(
                    'ajaxurl' => admin_url('admin-ajax.php'),
                    'check_nonce' => wp_create_nonce('rml-nonce'),
                    'posts' => json_encode($wp_query->query_vars),
                )
            );
        }

        /* end class */
    }

    new Class_Ajax;