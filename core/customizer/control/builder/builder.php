<?php

/*=================================================
 * KIRKI CUSTOM CONTROL
/*================================================= */
add_action('customize_register', function ($wp_customize) {
    /**
     * The custom control class
     */
    class Retheme_Header_Control extends Kirki_Control_Base
    {
        /**
         * @access public
         * @var string
         */
        public $type = 'builder';
        /**
         * @access public
         * @var array
         */
        public $builder;

        public function enqueue()
        {
            // wp_enqueue_script('jquery');
            wp_enqueue_script('jquery-ui-core');
            wp_enqueue_script('jquery-ui-sortable');
            wp_enqueue_script('jquery-ui-draggable');
            wp_enqueue_script('jquery-ui-tabs');

            wp_enqueue_style('retheme-style-builder', get_template_directory_uri() . '/core/customizer/control/builder/builder.css', '', null, false);
            wp_enqueue_script('retheme-scripts-builder', get_template_directory_uri() . '/core/customizer/control/builder/builder.js', array('jquery'), null, true);
        }

        public function to_json()
        {
            parent::to_json();
            $this->json['builder'] = $this->builder;
        }

        /**
         * Array value to key
         *
         * @param string $choices
         * @param array $elements
         * @return void
         */
        public function filter_values($choices = '', $elements = array())
        {
            $filter_values = array();

            foreach ($elements as $key => $element) {
                if(!empty($this->choices[$choices][$element])){
                    $filter_values[$element] = $this->choices[$choices][$element];

                }
                
            }

            return $filter_values;
        }



        public function render_content()
        {
            $values = $this->value();
            $defaults = $this->setting->default;
            $choices = $this->choices;
            $args = wp_parse_args($values, $defaults);

            /* Topbar */
            $topbar_left = $this->filter_values('normal_elements', $args['topbar_left_element']);
            $topbar_center = $this->filter_values('normal_elements', $args['topbar_center_element']);
            $topbar_right = $this->filter_values('normal_elements', $args['topbar_right_element']);
            $topbar = array_merge($topbar_left, $topbar_center, $topbar_right);

            /** Middle */
            $middle_left = $this->filter_values('normal_elements', $args['middle_left_element']);
            $middle_center = $this->filter_values('normal_elements', $args['middle_center_element']);
            $middle_right = $this->filter_values('normal_elements', $args['middle_right_element']);
            $middle = array_merge($middle_left, $middle_center, $middle_right);

            /** Main */
            $main_left = $this->filter_values('normal_elements', $args['main_left_element']);
            $main_center = $this->filter_values('normal_elements', $args['main_center_element']);
            $main_right = $this->filter_values('normal_elements', $args['main_right_element']);
            $main = array_merge($main_left, $main_center, $main_right);

            /** Merge all Element in normal header */
            $header_normal_element_exits = array_diff($choices['normal_elements'], array_merge($topbar, $middle, $main));

            /** Sticky */
            $sticky_left = $this->filter_values('sticky_elements', $args['sticky_left_element']);
            $sticky_center = $this->filter_values('sticky_elements', $args['sticky_center_element']);
            $sticky_right = $this->filter_values('sticky_elements', $args['sticky_right_element']);

            $header_sticky_element_exits = array_diff($choices['sticky_elements'], array_merge($sticky_left, $sticky_center, $sticky_right));

            /** Mobile */
            $mobile_left = $this->filter_values('mobile_elements', $args['mobile_left_element']);
            $mobile_center = $this->filter_values('mobile_elements', $args['mobile_center_element']);
            $mobile_right = $this->filter_values('mobile_elements', $args['mobile_right_element']);
            $header_mobile_element_exits = array_diff($choices['mobile_elements'], array_merge($mobile_left, $mobile_center, $mobile_right));

            /** Drawer */
            $drawer = $this->filter_values('drawer_elements', $args['drawer_element']);
            $drawer_element_exits = array_diff($choices['drawer_elements'], $drawer);

            ?>

            <div class="rt-builder js-builder">

  
                <div class="rt-builder__header">

                    <h3 class="rt-builder__title"><i class="fa fa-cubes"></i> <?php _e('Header Builder', 'rt_domain')?></h3>
                    
                    <?php if(rt_is_feature()): ?>
                    <div class="rt-builder__display js-builder-preview">
                      
                        <a href="#" id="header-builder-close" class="js-header-builder-close">
                            <i class="fa fa-times-circle"></i> <?php _e('Close', 'rt_domain')?>
                        </a>
                        <a href="#" id="header-builder-open" class="js-header-builder-trigger">
                            <i class="fa fa-cubes"></i> <?php _e('Header Builder', 'rt_domain')?>
                        </a>

                    </div>
                    <?php endif ?>

                </div>

                <?php if(rt_is_free()): ?>
                <div class="rt-builder__notif">Upgrade to <a href="<?php echo rt_var('pruduct-url')?>" target="_blank"><?php echo rt_var('product-name')?> Pro</a> to get more builder items and other premium features.</div>
                <?php endif ?>

                <div id="tabs">

                    <ul class="rt-builder__nav">
                        <li>
                            <a href="#header-tab-normal" class="js-preview-devices" data-device="desktop"><?php _e('Normal', 'rt_domain')?></a>
                        </li>

                        <?php if(rt_is_premium()): ?>
                        <li>
                            <a href="#header-tab-sticky" class="js-preview-devices" data-device="desktop"><?php _e('Sticky Header', 'rt_domain')?></a>
                        </li>
                        <?php endif ?>

                        <li>
                            <a href="#header-tab-mobile" class="js-preview-devices" data-device="tablet"><?php _e('Mobile Header', 'rt_domain')?></a>
                        </li>
                         <li>
                            <a href="#header-tab-drawer" class="js-preview-devices" data-device="tablet"><?php _e('Drawer', 'rt_domain')?></a>
                        </li>
                    </ul>

                    <div class="rt-builder__body">

                        <!-- Normal Header -->
                        <div id="header-tab-normal" class="rt-builder__tab">
                            <!-- topbar -->
                             <?php if(rt_is_premium()): ?>
                            <div id="header-topbar" class="rt-builder__row <?php echo !(rt_option('header_topbar')) ? 'disable' : '' ?>">

                                 <div class="rt-builder__row-title js-builder-control-focus" data-control="header_topbar" data-class="header_topbar">
                                     <?php echo '<i class="fa fa-gear"></i> ' . __('Topbar Header', 'rt_domain') ?> <?php echo !(rt_option('header_topbar')) ? '<span class="disable"> - Disable </span>' : '' ?>
                                 </div>

                                <div class="rt-builder__column left" data-alignment="<?php echo $args['topbar_left_alignment'] ?>" data-display="<?php echo $args['topbar_left_display'] ?>">
                                    <div id="header-topbar-left" class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($topbar_left as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                                <div class="rt-builder__column center" data-alignment="<?php echo $args['topbar_center_alignment'] ?>" data-display="<?php echo $args['topbar_center_display'] ?>">
                                    <div id="header-topbar-center" class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($topbar_center as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                                <div class="rt-builder__column right" data-alignment="<?php echo $args['topbar_right_alignment'] ?>" data-display="<?php echo $args['topbar_right_display'] ?>">
                                    <div id="header-topbar-right" class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($topbar_right as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                            </div>
                            <?php endif ?>

                            <!-- middle -->
                             <?php if(rt_is_premium()): ?>
                            <div id="header-middle" class="rt-builder__row  <?php echo !(rt_option('header_middle')) ? 'disable' : '' ?>">

                                <div class="rt-builder__row-title js-builder-control-focus" data-control="header_middle" data-class="header_middle">
                                    <?php echo '<i class="fa fa-gear"></i> ' . __('Middle Header', 'rt_domain') ?> <?php echo !(rt_option('header_middle')) ? '<span class="disable"> - Disable </span>' : '' ?>
                                </div>

                                <div class="rt-builder__column left" data-alignment="<?php echo $args['middle_left_alignment'] ?>" data-display="<?php echo $args['middle_left_display'] ?>">
                                    <div id="header-middle-left" class="sortable-wrapper js-builder-connect">
                                         <?php foreach ($middle_left as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                                <div class="rt-builder__column center" data-alignment="<?php echo $args['middle_center_alignment'] ?>" data-display="<?php echo $args['middle_center_display'] ?>">
                                    <div id="header-middle-center"  class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($middle_center as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                                <div class="rt-builder__column right" data-alignment="<?php echo $args['middle_right_alignment'] ?>" data-display="<?php echo $args['middle_right_display'] ?>">
                                    <div id="header-middle-right"  class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($middle_right as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                            </div>
                            <?php endif ?>

                            <!-- main -->
                            <div id="header-main" class="rt-builder__row <?php //echo !(rt_option('header_main'))? 'disable' :''?>">

                                <div class="rt-builder__row-title js-builder-control-focus" data-control="header_main" data-class="header_main">
                                    <?php echo '<i class="fa fa-gear"></i> ' . __('Main Header', 'rt_domain') ?> <?php //echo !(rt_option('header_main')) ? '<span class="disable"> - Disable </span>' : '' ?>
                                </div>

                                <div  class="rt-builder__column left" data-alignment="<?php echo $args['main_left_alignment'] ?>" data-display="<?php echo $args['main_left_display'] ?>">
                                    <div id="header-main-left"  class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($main_left as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                                <div class="rt-builder__column center" data-alignment="<?php echo $args['main_center_alignment'] ?>" data-display="<?php echo $args['main_center_display'] ?>">
                                    <div id="header-main-center" class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($main_center as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                                <div class="rt-builder__column right" data-alignment="<?php echo $args['main_right_alignment'] ?>" data-display="<?php echo $args['main_right_display'] ?>">
                                    <div id="header-main-right" class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($main_right as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                            </div>

                            <!-- element -->
                            <div class="rt-builder__row rt-builder__row--source">

                                <div class="rt-builder__column">
                                    <div class="sortable-wrapper js-builder-connect js-builder-source">
                                       <?php foreach ($header_normal_element_exits as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <!-- Sticky Header -->
                        <?php if(rt_is_premium()): ?>
                        <div id="header-tab-sticky" class="rt-builder__tab">

                            <div id="header-sticky" class="rt-builder__row <?php echo !(rt_option('header_sticky')) ? 'disable' : '' ?>">

                                 <div class="rt-builder__row-title js-builder-control-focus" data-control="header_sticky" data-class="header_sticky">
                                     <?php echo '<i class="fa fa-gear"></i> ' . __('Sticky Header', 'rt_domain') ?> <?php echo !(rt_option('header_sticky')) ? '<span class="disable"> - Disable </span>' : '' ?>
                                </div>

                                <div class="rt-builder__column left" data-alignment="<?php echo $args['sticky_left_alignment'] ?>" data-display="<?php echo $args['sticky_left_display'] ?>">
                                    <div id="header-sticky-left" class="sortable-wrapper js-builder-connect">
                                         <?php foreach ($sticky_left as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                                <div class="rt-builder__column center" data-alignment="<?php echo $args['sticky_center_alignment'] ?>" data-display="<?php echo $args['sticky_center_display'] ?>">
                                    <div id="header-sticky-center" class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($sticky_center as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                                <div class="rt-builder__column right" data-alignment="<?php echo $args['sticky_right_alignment'] ?>" data-display="<?php echo $args['sticky_right_display'] ?>">
                                    <div id="header-sticky-right" class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($sticky_right as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                            </div>

                            <!-- element -->
                            <div class="rt-builder__row rt-builder__row--source">

                                <div class="rt-builder__column">
                                    <div class="sortable-wrapper js-builder-connect js-builder-source">
                                         <?php foreach ($header_sticky_element_exits as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <?php endif ?>

                        <!-- Mobile Header -->
                        <div id="header-tab-mobile" class="rt-builder__tab">

                            <div id="header-mobile" class="rt-builder__row">

                                 <div class="rt-builder__row-title js-builder-control-focus" data-control="header_mobile_menu" data-class="header_mobile">
                                     <?php echo '<i class="fa fa-gear"></i> ' . __('Mobile Header', 'rt_domain') ?>
                                </div>

                                <div class="rt-builder__column left" data-alignment="<?php echo $args['mobile_left_alignment'] ?>" data-display="<?php echo $args['mobile_left_display'] ?>">
                                    <div id="header-mobile-left" class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($mobile_left as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                                <div class="rt-builder__column center" data-alignment="<?php echo $args['mobile_center_alignment'] ?>" data-display="<?php echo $args['mobile_center_display'] ?>">
                                    <div id="header-mobile-center" class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($mobile_center as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                                <div class="rt-builder__column right" data-alignment="<?php echo $args['mobile_right_alignment'] ?>" data-display="<?php echo $args['mobile_right_display'] ?>">
                                    <div id="header-mobile-right" class="sortable-wrapper js-builder-connect">
                                           <?php foreach ($mobile_right as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                    <span class="setting js-builder-column-setting-trigger">
                                        <i class="fa fa-gear"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="rt-builder__row rt-builder__row--source">

                                <div class="rt-builder__column">
                                    <div class="sortable-wrapper js-builder-connect js-builder-source">
                                        <?php foreach ($header_mobile_element_exits as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>

                                </div>

                            </div>

                        </div>

                         <!-- Drawer -->
                        <div id="header-tab-drawer" class="rt-builder__tab">

                            <div id="header-drawer" class="rt-builder__row">

                                <div class="rt-builder__row-title js-builder-control-focus" data-control="header_drawer_menu" data-class="menu_drawer">
                                    <?php echo '<i class="fa fa-gear"></i> ' . __('Drawer Menu', 'rt_domain') ?>
                                </div>

                                <div class="rt-builder__column rt-builder__column--drawer-source rt-builder__column--drawer ">
                                    <div id="header-drawer-drop" class="sortable-wrapper js-builder-connect js-builder-source">
                                        <?php foreach ($drawer_element_exits as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                </div>

                                <div class="rt-builder__column rt-builder__column--drawer">
                                    <div id="drawer_element" class="sortable-wrapper js-builder-connect">
                                        <?php foreach ($drawer as $key => $element): ?>
                                            <div id="<?php echo $key ?>" class="rt-builder-element <?php echo $key ?>"><?php echo $element ?> <i class="fa fa-times p-close js-builder-element-close"></i></div>
                                        <?php endforeach;?>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <!-- tabs ends -->
                    </div>

                </div>

            </div>

            <?php

        }


    }

   

    // Register our custom control with Kirki
    add_filter('kirki_control_types', function ($controls) {
        $controls['builder'] = 'Retheme_Header_Control';
        return $controls;
    });

});
