<?php

namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Header extends Customizer_Base
{
    public function __construct()
    {
        $this->set_panel();
        $this->set_section();

        $this->add_header_builder();

        /** desktop options */
        $this->add_options_main();

        /** Main menu */
        $this->add_main_menu();
        $this->add_main_submenu();

        /** Mobile menu */
        $this->add_mobile_header();
        $this->add_mobile_header_sticky();
        $this->add_drawer_menu();
        $this->add_drawer_search();

        /** Header Element */
        $this->add_element_social();
        $this->add_element_search_form();
        $this->add_element_button();
        $this->add_element_html();

    }

    public function set_panel()
    {
        $this->add_panel('header_panel', array(
            'title' => __('Header', 'rt_domain'),
        ));
    }

    public function set_section()
    {

        $this->add_section('header_panel', array(
            'header_desktop' => array(__('Desktop Options', 'rt_domain')),
            'header_mobile' => array(__('Mobile Options', 'rt_domain')),
            'header_overlay' => array(__('Header Overlay', 'rt_domain')),

            'header_main_menu' => array(__('Main Menu', 'rt_domain')),
            'header_mobile_menu' => array(__('Mobile Menu', 'rt_domain')),

            'header_socmed' => array(__('Social Media', 'rt_domain')),
            'header_button' => array(__('Button', 'rt_domain')),
            'header_html' => array(__('HTML', 'rt_domain')),
            'header_search' => array(__('Search Form', 'rt_domain')),
            'header_cart' => array(__('Cart', 'rt_domain')),

            'header_builder' => array(__('Builder', 'rt_domain')),

        ));
    }

    public function get_header_element($partial = '')
    {
        $elements = get_theme_mod("header_builder_option")[$partial];

        if (is_array($elements)) {
            foreach ($elements as $key => $element) {
                rt_get_template_part("header/element/" . $element);
            }
        }
    }

    public function add_header_builder()
    {

        $elements = apply_filters('rt_builder_element', array(
            'logo' => 'Logo',
            'main-menu' => 'Main Menu',
            'cart-icon' => 'Cart Icon',
            'user-icon' => 'User Icon',
            'search-icon' => 'Search Icon',
            'social' => 'Social Media',
            'html-1' => 'HTML 1',
            'html-2' => 'HTML 2',
        ));

        $mobile_elements = apply_filters('rt_builder_mobile_element', array(
            'logo-mobile' => 'Logo',
            'toggle-menu' => 'Toggle Menu',
            'cart-icon' => 'Cart Icon',
            'user-icon' => 'User Icon',
            'search-icon' => 'Search Icon',
            'html-1' => 'HTML 1',
            'html-2' => 'HTML 2',
        ));

        $drawer_elements = apply_filters('rt_builder_drawer', array(
            'main-menu' => 'Main Menu',
            'search-form' => 'Search Form',
            'social' => 'Social Media',
            'html-1' => 'HTML 1',
            'html-2' => 'HTML 2',
        ));

        $this->add_field(array(
            'label' => 'Main Bar',
            'type' => 'builder',
            'settings' => 'header_builder_option',
            'section' => 'header_builder_section',
            'default' => rt_var('header-default'),

            'choices' => array(
                'normal_elements' => $elements,
                'sticky_elements' => $elements,
                'mobile_elements' => $mobile_elements,
                'drawer_elements' => $drawer_elements,

            ),
            'partial_refresh' => array(

                'header_topbar_left' => array(
                    'selector' => '#header-topbar-left',
                    'render_callback' => function () {
                        return $this->get_header_element('topbar_left_element');
                    },
                ),

                'header_topbar_center' => array(
                    'selector' => '#header-topbar-center',
                    'render_callback' => function () {
                        return $this->get_header_element('topbar_center_element');
                    },
                ),

                'header_topbar_right' => array(
                    'selector' => '#header-topbar-right',
                    'render_callback' => function () {
                        return $this->get_header_element('topbar_right_element');
                    },
                ),

                'header_middle_left' => array(
                    'selector' => '#header-middle-left',
                    'render_callback' => function () {
                        return $this->get_header_element('middle_left_element');
                    },
                ),

                'header_middle_center' => array(
                    'selector' => '#header-middle-center',
                    'render_callback' => function () {
                        return $this->get_header_element('middle_center_element');
                    },
                ),

                'header_middle_right' => array(
                    'selector' => '#header-middle-right',
                    'render_callback' => function () {
                        return $this->get_header_element('middle_right_element');
                    },
                ),

                'header_main_left' => array(
                    'selector' => '#header-main-left',
                    'render_callback' => function () {
                        return $this->get_header_element('main_left_element');
                    },
                ),

                'header_main_center' => array(
                    'selector' => '#header-main-center',
                    'render_callback' => function () {
                        return $this->get_header_element('main_center_element');
                    },
                ),

                'header_main_right' => array(
                    'selector' => '#header-main-right',
                    'render_callback' => function () {
                        return $this->get_header_element('main_right_element');
                    },
                ),

                'header_sticky_left' => array(
                    'selector' => '#header-sticky-left',
                    'render_callback' => function () {
                        return $this->get_header_element('sticky_left_element');
                    },
                ),

                'header_sticky_center' => array(
                    'selector' => '#header-sticky-center',
                    'render_callback' => function () {
                        return $this->get_header_element('sticky_center_element');
                    },
                ),

                'header_sticky_right' => array(
                    'selector' => '#header-sticky-right',
                    'render_callback' => function () {
                        return $this->get_header_element('sticky_right_element');
                    },
                ),

                'header_mobile_left' => array(
                    'selector' => '#header-mobile-left',
                    'render_callback' => function () {
                        return $this->get_header_element('mobile_left_element');
                    },
                ),

                'header_mobile_center' => array(
                    'selector' => '#header-mobile-center',
                    'render_callback' => function () {
                        return $this->get_header_element('mobile_center_element');
                    },
                ),

                'header_mobile_right' => array(
                    'selector' => '#header-mobile-right',
                    'render_callback' => function () {
                        return $this->get_header_element('mobile_right_element');
                    },
                ),

                'header_drawer' => array(
                    'selector' => '#header-mobile-drawer',
                    'render_callback' => function () {
                        return $this->get_header_element('drawer_element');
                    },
                ),

            ),

        ));
    }

    public function add_options_main()
    {
        $this->add_header(array(
            'label' => 'Main Bar',
            'settings' => 'header_main',
            'section' => 'header_desktop_section',
            'class' => 'header_main',
        ));

        $this->add_field(array(
            'settings' => 'header_main_height',
            'type' => 'slider',
            'label' => __('Height', 'rt_domain'),
            'section' => 'header_desktop_section',
            'class' => 'header_main',
            'default' => '60',
            'choices' => array(
                'min' => '45',
                'max' => '150',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header__main',
                    'property' => 'height',
                    'units' => 'px',
                ),
                array(
                    'element' => '.rt-header__main .rt-menu--horizontal',
                    'property' => 'height',
                    'units' => 'px',
                ),
                array(
                    'element' => '.rt-header .rt-search--overlay',
                    'property' => 'height',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_field_link(array(
            'settings' => 'header_main_link',
            'section' => 'header_desktop_section',
            'class' => 'header_main',
            'element' => '.rt-header__main .rt-header__html a,
	                   .rt-header__main .rt-header__element i,
	                   .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > a,
                       .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > .rt-menu__arrow i,

                       .rt-header__sticky .rt-header__html a,
	                   .rt-header__sticky .rt-header__element i,
	                   .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > a,
	                   .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item > .rt-menu__arrow i',

        ));

        $this->add_field(array(
            'settings' => 'header_main_link_hover',
            'section' => 'header_desktop_section',
            'class' => 'header_main',
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'label' => __('Link :Hover', 'rt_domain'),
            'output' => array(
                array(
                    'element' => '.rt-header__main .rt-header__html a:hover,
                       .rt-header__main .rt-header__element i:hover,
                       .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > a,
                       .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > .rt-menu__arrow i,

                       .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > a,
                       .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > .rt-menu__arrow i,

                       .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > a,
                       .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > .rt-menu__arrow.is-active i,

                       .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > a,
                       .rt-header__main .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > .rt-menu__arrow.is-active i,

                       .rt-header__sticky .rt-header__html a:hover,
                       .rt-header__sticky .rt-header__element i:hover,
                       .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > a,
                       .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.is-active > .rt-menu__arrow i,

                       .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > a,
                       .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-parent > .rt-menu__arrow i,

                       .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > a,
                       .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current-menu-item > .rt-menu__arrow.is-active i,

                       .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > a,
                       .rt-header__sticky .rt-menu--horizontal .rt-menu__main > li.rt-menu__item.current_page_item > .rt-menu__arrow.is-active i',

                    'property' => 'color',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field_background(array(
            'settings' => 'header_main_background',
            'section' => 'header_desktop_section',
            'class' => 'header_main',
            'element' => '.rt-header__main, .rt-header__sticky',
        ));

        $this->add_field_border_color(array(
            'settings' => 'header_main_border_color',
            'section' => 'header_desktop_section',
            'class' => 'header_main',
            'element' => '.rt-header__main, .rt-header__sticky',
        ));

        $this->add_field_background(array(
            'label' => 'Color Divender',
            'settings' => 'header_main_color_divender',
            'section' => 'header_desktop_section',
            'class' => 'header_main',
            'element' => '.rt-header__main .rt-header__divender, .rt-header__sticky .rt-header__divender',
        ));
    }

    public function add_options_sticky()
    {

    }

    public function add_options_topbar()
    {

    }

    public function add_options_middle()
    {

    }

    public function add_options_overlay()
    {
        $section = 'header_overlay_section';

        $this->add_header(array(
            'label' => 'Header Overlay',
            'settings' => 'header_overlay',
            'section' => $section,
            'class' => 'header_main',
        ));

        $this->add_field(array(
            'settings' => 'header_overlay_height',
            'type' => 'slider',
            'label' => __('Height', 'rt_domain'),
            'section' => $section,
            'class' => 'header_main',
            'default' => '60',
            'choices' => array(
                'min' => '45',
                'max' => '150',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header.is-overlay .rt-header__main',
                    'property' => 'height',
                    'units' => 'px',
                ),
                array(
                    'element' => '.rt-menu--horizontal',
                    'property' => 'height',
                    'units' => 'px',
                ),

                array(
                    'element' => '.rt-header.is-overlay .rt-header .rt-search--overlay',
                    'property' => 'height',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'settings' => 'header_overlay_link',
            'section' => $section,
            'class' => 'header_main',
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'label' => __('Link', 'rt_domain'),
            'output' => array(
                array(
                    'element' => '.rt-header.is-overlay .rt-header__main .rt-header__html a,
	                   .rt-header.is-overlay .rt-header__main .rt-header__element i,
	                   .rt-header.is-overlay .rt-header__main .rt-menu__main > li.rt-menu__item > a,
                       .rt-header.is-overlay .rt-header__main .rt-menu__main > li.rt-menu__item > .rt-menu__arrow i',
                    'property' => 'color',
                ),
                array(
                    'element' => '.rt-header-mobile.is-overlay .rt-menu-toggle span,
                       .rt-header-mobile.is-overlay .rt-menu-toggle span::after,
                       .rt-header-mobile.is-overlay .rt-menu-toggle span::before',
                    'property' => 'background-color',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'settings' => 'header_overlay_link_hover',
            'section' => $section,
            'class' => 'header_main',
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'label' => __('Link :Hover', 'rt_domain'),
            'output' => array(
                array(
                    'element' => '.rt-header.is-overlay .rt-header__main .rt-header__html a:hover,
	                   .rt-header.is-overlay .rt-header__main .rt-header__element i:hover,
	                   .rt-header.is-overlay .rt-header__main .rt-menu__main > li.rt-menu__item.is-active > a,
                       .rt-header.is-overlay .rt-header__main .rt-menu__main > li.rt-menu__item.is-active > .rt-menu__arrow i,
                       .rt-header.is-overlay .rt-header__main .rt-menu__main  > li.rt-menu__item.current-menu-item a',
                    'property' => 'color',
                ),
                array(
                    'element' => '.rt-header-mobile.is-overlay .rt-menu-toggle.is-active span,
                       .rt-header-mobile.is-overlay .rt-menu-toggle.is-active span::after,
                       .rt-header-mobile.is-overlay .rt-menu-toggle.is-active span::before',
                    'property' => 'background-color',
                ),
            ),
            'transport' => 'auto',

        ));

        $this->add_field_background(array(
            'settings' => 'header_overlay_background',
            'section' => $section,
            'class' => 'header_main',
            'element' => '.rt-header.is-overlay .rt-header__main,
                            .rt-header-mobile.is-overlay .rt-header-mobile__main',
        ));

        $this->add_field_border_color(array(
            'settings' => 'header_overlay_border_color',
            'section' => $section,
            'class' => 'header_main',
            'element' => '.rt-header.is-overlay .rt-header__main',
        ));

        $this->add_field_background(array(
            'label' => 'Color Divender',
            'settings' => 'header_overlay_color_divender',
            'section' => $section,
            'class' => 'header_main',
            'element' => '.rt-header.is-overlay .rt-header__main .rt-header__divender',
        ));

        $this->add_field_background(array(
            'label' => 'Background Effect',
            'settings' => 'header_overlay_color_effect',
            'section' => $section,
            'class' => 'header_main',
            'element' => '.rt-header.is-overlay .rt-menu--horizontal .rt-menu__item a::before',
        ));
    }

    public function add_layout_prebuilder()
    {

        /**
         * get list from elementor library
         */
        $library = \Retheme\Helper::get_posts('elementor_library');
        array_unshift($library, 'Select Header Library');

        $this->add_field(array(
            'type' => 'select',
            'section' => 'header_layout_section',
            'settings' => 'header_layout_builder_elementor',
            'label' => __('Elementor Library', 'rt_domain'),
            'multiple' => 1,
            'choices' => $library,
            'active_callback' => array(
                array(
                    'setting' => 'header_layout_builder',
                    'operator' => '==',
                    'value' => array('header-builder'),
                ),
            ),

        ));
    }

    public function add_main_menu()
    {
        $section = 'header_main_menu_section';

        $this->add_header(array(
            'label' => 'Menu',
            'settings' => 'header_main_menu',
            'section' => $section,
            'class' => 'header_menu',
        ));

        $this->add_field(array(
            'type' => 'typography',
            'settings' => 'header_main_menu_typography',
            'label' => __('Typography', 'rt_domain'),
            'section' => $section,
            'class' => 'header_menu',
            'default' => array(
                'font-family' => get_theme_mod('general_heading_typography', rt_var('font-primary')),
                'variant' => '',
                'font-size' => '',
                'text-transform' => 'none',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header .rt-menu--horizontal .rt-menu__item>a',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'settings' => 'header_main_menu_menu_bg',
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'label' => __('Background Effect', 'rt_domain'),
            'section' => $section,
            'class' => 'header_menu',
            'output' => array(
                array(
                    'element' => '.rt-header .rt-menu--horizontal .rt-menu__item a::before',
                    'property' => 'background-color',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field(array(
            'settings' => 'header_main_menu_menu_gap',
            'type' => 'slider',
            'label' => __('Menu Gap', 'rt_domain'),
            'section' => $section,
            'choices' => array(
                'min' => '5',
                'max' => '30',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header .rt-menu.rt-menu--horizontal:not(#topbar-menu) .rt-menu__item a',
                    'property' => 'padding-left',
                    'units' => 'px',
                ),
                array(
                    'element' => '.rt-header .rt-menu.rt-menu--horizontal:not(#topbar-menu) .rt-menu__item a',
                    'property' => 'padding-right',
                    'units' => 'px',
                ),

            ),
            'transport' => 'auto',
        ));
    }

    public function add_main_submenu()
    {
        $section = 'header_main_menu_section';

        $this->add_header(array(
            'label' => 'Sub Menu',
            'settings' => 'header_main_submenu',
            'section' => $section,
            'class' => 'header_submenu',
        ));
        $this->add_field(array(
            'type' => 'typography',
            'settings' => 'header_main_submenu_typography',
            'label' => __('Typography', 'rt_domain'),
            'section' => $section,
            'class' => 'header_submenu',
            'default' => array(
                'font-family' => get_theme_mod('general_heading_typography', rt_var('font-primary')),
                'variant' => '',
                'font-size' => '',
                'text-transform' => 'none',
            ),
            'output' => array(
                array(
                    'element' => '.rt-header .rt-menu--horizontal .rt-menu__submenu .rt-menu__item a',
                ),
            ),
            'transport' => 'auto',

        ));

    }

    public function add_mobile_header()
    {
        $section = 'header_mobile_section';

        $this->add_header(array(
            'label' => 'Header',
            'settings' => 'header_mobile_menu',
            'section' => $section,
            'class' => 'header_mobile',

        ));

        $this->add_field_background(array(
            'settings' => 'header_mobile_menu_background',
            'section' => $section,
            'class' => 'header_mobile',
            'element' => '.rt-header-mobile__main',
        ));

        $this->add_field_border_color(array(
            'settings' => 'header_mobile_menu_border_color',
            'section' => $section,
            'class' => 'header_mobile',
            'element' => '.rt-header-mobile__main',
        ));

        $this->add_field_color(array(
            'settings' => 'header_mobile_menu_menu_link',
            'section' => $section,
            'class' => 'header_mobile',
            'element' => '.rt-header-mobile__main i',
            'pseudo' => 'hover',
        ));

        $this->add_field_background(array(
            'label' => 'Link Bar Color',
            'settings' => 'header_mobile_menu_menu_bar',
            'section' => $section,
            'class' => 'header_mobile',
            'element' => '.rt-header-mobile__main .rt-menu-toggle span,
                    .rt-header-mobile__main .rt-menu-toggle span:after,
                    .rt-header-mobile__main .rt-menu-toggle span:before',
        ));

        $this->add_field_background(array(
            'label' => 'Color Divender',
            'settings' => 'header_mobile_menu_color_divender',
            'section' => $section,
            'class' => 'header_mobile',
            'element' => '.rt-header-mobile__main .rt-header__divender',
        ));
    }

    public function add_mobile_header_sticky()
    {

    }

    public function add_drawer_menu()
    {

    }

    public function add_drawer_search()
    {

    }

    public function add_element_social()
    {
    }

    public function add_element_button()
    {

    }

    public function add_element_search_form()
    {

    }

    public function add_element_html()
    {

        $section = 'header_html_section';

        if (rt_is_free()) {
            $this->add_header(array(
                'label' => "HTML 1",
                'settings' => "header_html_1",
                'section' => $section,
            ));

            $this->add_field(array(
                'label' => 'Input Your Text/HTML',
                'settings' => "header_html_1",
                'section' => $section,
                'type' => 'textarea',
            ));

            $this->add_field(array(
                'type' => 'fontawesome',
                'settings' => "header_html_1_icon",
                'label' => __('Icon', 'rt_domain'),
                'section' => $section,
            ));

            $this->add_field(array(
                'type' => 'toggle',
                'settings' => "header_html_1_shortcode",
                'label' => __('Enable Shortcode', 'rt_domain'),
                'section' => $section,
                'default' => '',
            ));

        }

    }

    public function add_cart_icon()
    {
        $section = 'header_cart_section';

        $this->add_field(array(
            'type' => 'select',
            'section' => $section,
            'settings' => 'header_cart_icon',
            'label' => __('Icon', 'rt_domain'),
            'multiple' => 1,
            'choices' => array(
                'cart-light' => 'Shopping Cart Light',
                'shopping-basket' => 'Shopping Cart Bold',
                'shopping-cart' => 'Shopping Cart Basket',
            ),

        ));
        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'header_cart_total',
            'label' => __('Subtotal', 'rt_domain'),
            'section' => $section,
            'default' => true,
        ));

    }

    // end class
}

new Header;
