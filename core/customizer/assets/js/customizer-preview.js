jQuery(document).ready(function ($) {



    var api = wp.customize;

    /*=================================================
    * HEADER
    /*================================================= */

    var hasSelectiveRefresh;

    hasSelectiveRefresh = (
        'undefined' !== typeof wp &&
        api &&
        api.selectiveRefresh &&
        api.widgetsPreview &&
        api.widgetsPreview.WidgetPartial
    );
    
    if (hasSelectiveRefresh) {


        api.selectiveRefresh.bind('partial-content-rendered', function (partial) {

            $('.js-menu, .js-mobile-menu').retheme_menu();
            $('.js-mobile-menu').hide();
            $('.js-tab').retheme_tabs();
            $('.js-search-trigger"').retheme_search();
            $('.js-sidepanel').retheme_sidepanel();
            $('.js-modal').retheme_modal();

        });
        
    }

      api('header_builder_option', function (value) {
          value.bind(function (newVal) {
              $('#header-topbar-left').attr('data-alignment', newVal.topbar_left_alignment);
              $('#header-topbar-left').attr('data-display', newVal.topbar_left_display);
              $('#header-topbar-center').attr('data-alignment', newVal.topbar_center_alignment);
              $('#header-topbar-center').attr('data-display', newVal.topbar_center_display);
              $('#header-topbar-right').attr('data-alignment', newVal.topbar_right_alignment);
              $('#header-topbar-right').attr('data-display', newVal.topbar_right_display);

              $('#header-middle-left').attr('data-alignment', newVal.middle_left_alignment);
              $('#header-middle-left').attr('data-display', newVal.middle_left_display);
              $('#header-middle-center').attr('data-alignment', newVal.middle_center_alignment);
              $('#header-middle-center').attr('data-display', newVal.middle_center_display);
              $('#header-middle-right').attr('data-alignment', newVal.middle_right_alignment);
              $('#header-middle-right').attr('data-display', newVal.middle_right_display);

              $('#header-main-left').attr('data-alignment', newVal.main_left_alignment);
              $('#header-main-left').attr('data-display', newVal.main_left_display);
              $('#header-main-center').attr('data-alignment', newVal.main_center_alignment);
              $('#header-main-center').attr('data-display', newVal.main_center_display);
              $('#header-main-right').attr('data-alignment', newVal.main_right_alignment);
              $('#header-main-right').attr('data-display', newVal.main_right_display);

              $('#header-mobile-left').attr('data-alignment', newVal.mobile_left_alignment);
              $('#header-mobile-left').attr('data-display', newVal.mobile_left_display);
              $('#header-mobile-center').attr('data-alignment', newVal.mobile_center_alignment);
              $('#header-mobile-center').attr('data-display', newVal.mobile_center_display);
              $('#header-mobile-right').attr('data-alignment', newVal.mobile_right_alignment);
              $('#header-mobile-right').attr('data-display', newVal.mobile_right_display);
          });
      });
       


    /*=================================================
    * BUTTON
    /*================================================= */
    api('header_button_1_text', function (value) {
        value.bind(function (newval) {
            $('.rt-btn--1 span').text(newval);
        });
    });

    api('header_button_2_text', function (value) {
        value.bind(function (newval) {
            $('.rt-btn--2 span').text(newval);
        });
    });

    api('header_button_3_text', function (value) {
        value.bind(function (newval) {
            $('.rt-btn--3 span').text(newval);
        });
    });



});