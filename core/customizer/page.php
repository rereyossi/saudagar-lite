<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Page extends Customizer_Base
{

    public function __construct()
    {

        $this->set_section();

        $this->add_page_settings();
        $this->add_page_title();
        $this->add_page_sub_title();

    }

    public function set_section()
    {
        $this->add_section('', array(
            'page_header' => array(esc_attr__('Page', 'rt_domain')),
        ));
    }

    public function add_page_settings()
    {

        $this->add_header(array(
            'label' => 'Options',
            'settings' => 'page_header',
            'section' => 'page_header_section',
            'class' => 'page_header',
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'page_header',
            'label' => __('Enable Page Header', 'rt_domain'),
            'section' => 'page_header_section',
            'class' => 'page_header',
            'default' => true,
        ));

      

        $this->add_field_background(array(
            'settings' => 'page_header_background',
            'section' => 'page_header_section',
            'class' => 'page_header',
            'element' => '.page-header',
        ));

        $this->add_field(array(
            'type' => 'background',
            'settings' => 'page_header_background',
            'section' => 'page_header_section',
            'class' => 'page_header',
            'label' => __('Background', 'rt_domain'),
            'default' => array(
                'background-color' => '',
                'background-image' => '',
                'background-repeat' => 'repeat',
                'background-position' => 'center center',
                'background-size' => 'cover',
                'background-attachment' => 'scroll',
            ),
            'output' => array(
                array(
                    'element' => '.page-header',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field_border_color(array(
            'settings' => 'page_header_border_color',
            'section' => 'page_header_section',
            'class' => 'page_header',
            'element' => '.page-header',
        ));

    

    }

    public function add_page_title()
    {

        $this->add_header(array(
            'label' => 'Title',
            'settings' => 'page_title',
            'section' => 'page_header_section',
            'class' => 'page_title',
        ));

  

        $this->add_field_color(array(
            'settings' => 'page_title_color',
            'section' => 'page_header_section',
            'class' => 'page_title',
            'element' => '.page-header__title',
        ));

    }

    public function add_page_sub_title()
    {

        $this->add_header(array(
            'label' => 'Description',
            'settings' => 'page_description',
            'section' => 'page_header_section',
            'class' => 'page_description',
        ));

   

        $this->add_field_color(array(
            'settings' => 'page_description_color',
            'section' => 'page_header_section',
            'class' => 'page_description',
            'element' => '.page-header__desc',
        ));

    }

// end class
}

new Page;
