<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class WooCommerce extends Customizer_Base
{

    public function __construct()
    {

        add_action('customize_register', array($this, 'remove_default_field'));

        $this->set_section();

        $this->add_archive_option();

        $this->add_single_option();
        $this->add_single_related();

        $this->add_sale_badge();
        $this->add_star_rating();

    }

    public function set_section()
    {
        $this->add_section('woocommerce', array(
            'woocommerce_archive' => array(__('Archive', 'rt_domain')),
            'woocommerce_single' => array(__('Product', 'rt_domain')),
            'woocommerce_catalog' => array(__('Catalog', 'rt_domain')),
            'woocommerce_badge' => array(__('Badge Sale', 'rt_domain')),
            'woocommerce_rating' => array(__('Star Rating', 'rt_domain')),
            'woocommerce_checkout' => array(__('Checkout', 'rt_domain')),
            'woocommerce_my_account' => array(__('My Account', 'rt_domain')),

        ));
    }

    /**
     * Remove default customizer woocommerce
     *
     * @param [type] $wp_customize customizer control
     * @return void
     */
    public function remove_default_field($wp_customize)
    {
        $wp_customize->add_control('woocommerce_catalog_columns')->theme_supports = false;
        $wp_customize->add_control('woocommerce_catalog_rows')->theme_supports = false;
    }


    public function add_archive_option()
    {
        $section = 'woocommerce_archive_section';
        $settings = 'woocommerce_archive_options';


        $this->add_field_responsive(array(
            'type' => 'select',
            'section' => 'woocommerce_archive_section',
            'settings' => 'woocommerce_archive_options_column',
            'label' => __('Column', 'rt_domain'),
            'default' => 3,
            'multiple' => 1,
            'choices' => array(
                1 => __('1 Column', 'rt_domain'),
                2 => __('2 Column', 'rt_domain'),
                3 => __('3 Column', 'rt_domain'),
                4 => __('4 Column', 'rt_domain'),
                6 => __('6 Column', 'rt_domain'),
            ),

        ));

  

        $this->add_field_responsive(array(
            'type' => 'number',
            'section' => $section,
            'settings' => 'woocommerce_archive_options_post_per_page',
            'label' => __('Post Per Page', 'rt_domain'),
            'default' => 9,
            'choices' => array(
                'min' => 1,
                'max' => 20,
            ),

        ));



        $this->add_header(array(
            'label' => 'Mini Cart',
            'settings' => 'woocommerce_ajax_mini_cart',
            'section' => $section,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'woocommerce_ajax_mini_cart',
            'label' => __('Ajax Mini Cart', 'rt_domain'),
            'section' => $section,
            'default' => true,
        ));

    }

    public function add_single_option()
    {
        $section = 'woocommerce_single_section';

        $this->add_header(array(
            'label' => 'Options',
            'settings' => 'woocommerce_single_options',
            'section' => $section,
            'class' => 'woocommerce_single',
        ));

        // remove add to cart button
         $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'woocommerce_catalog',
            'label' => __('Enable Catalog', 'rt_domain'),
            'description' => 'Hide Add to Cart button from homepage and all other pages',
            'section' => $section,
            'default' => false,
        ));


        $this->add_field(array(
            'type' => 'toggle',
            'label' => 'Product Meta',
            'settings' => 'woocommerce_single_meta',
            'section' => $section,
            'default' => true,
        ));

        $this->add_field(array(
            'type' => 'toggle',
            'label' => 'Share Button',
            'settings' => 'woocommerce_single_share',
            'section' => $section,
            'default' => true,
        ));

    }

    public function add_single_related()
    {
        $section = 'woocommerce_single_section';
        $settings = 'woocommerce_single_related';

        $this->add_header(array(
            'label' => 'Related Product',
            'settings' => 'woocommerce_single_related',
            'section' => $section,
        ));

        $this->add_field(array(
            'type' => 'select',
            'settings' => $settings . '_count',
            'label' => __('Related Count', 'rt_domain'),
            'section' => $section,
            'default' => 4,
            'multiple' => 1,
            'choices' => array(
                2 => __('2', 'rt_domain'),
                3 => __('3', 'rt_domain'),
                4 => __('4', 'rt_domain'),
                6 => __('6', 'rt_domain'),
            ),
        ));

        $this->add_field_responsive(array(
            'type' => 'select',
            'settings' => $settings . '_show',
            'label' => __('Slider Show', 'rt_domain'),
            'section' => $section,
            'default' => 4,
            'multiple' => 1,
            'choices' => array(
                1 => __('1', 'rt_domain'),
                2 => __('2', 'rt_domain'),
                3 => __('3', 'rt_domain'),
                4 => __('4', 'rt_domain'),
                6 => __('6', 'rt_domain'),
            ),
        ));
    }

    /**
     * sale badge in image thumbnail
     *
     * @return void
     */
    public function add_sale_badge()
    {
      
    }

    public function add_star_rating()
    {
      
    }

    public function add_table()
    {
        $this->add_field_border_radius(array(
            'settings' => 'woocommerce_table',
            'section' => 'woocommerce_element_section',
            'element' => '.woocommerce table.shop_table',
        ));

    }

// end class
}

new WooCommerce;
