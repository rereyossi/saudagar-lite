<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class General extends Customizer_Base
{

    public function __construct()
    {
        $this->set_panel();

        $this->set_section();

        $this->add_typography();
        $this->add_color();

        $this->add_breadcrumbs();
        $this->add_topup();

        $this->add_button_primary();

        if (class_exists('woocommerce')) {
            $this->add_button_second();
            $this->add_button_action();
        }

        $this->add_pagination_number();
        $this->add_pagination_loadmore();

    }

    public function set_panel()
    {
        $this->add_panel('general_panel', array(
            'title' => __('General', 'rt_domain'),
        ));
    }

    public function set_section()
    {
        $this->add_section('general_panel', array(
            'general_typography' => array(esc_attr__('Typography', 'rt_domain')),
            'general_color' => array(esc_attr__('Background & Color', 'rt_domain')),
            'general_breadcrumbs' => array(esc_attr__('Breadcrumbs', 'rt_domain')),
            'general_topup' => array(esc_attr__('Top Up', 'rt_domain')),
            'general_pagination' => array(esc_attr__('Pagination', 'rt_domain')),
            'general_button' => array(esc_attr__('Button', 'rt_domain')),

        ));
    }

    public function add_typography()
    {

        $this->add_field(array(
            'label' => __('All Heading', 'rt_domain'),
            'type' => 'typography',
            'settings' => 'typography_heading',
            'section' => 'general_typography_section',
            'default' => array(
                'font-family' => rt_var('font-primary'),
                'color' => rt_var('color-primary'),
                'variant' => rt_var('font-weight'),
                'text-transform' => 'none',
            ),
            'output' => array(
                array(
                    'element' => 'h1, h2, h3, h4, h5, h6,
                                .woocommerce ul.cart_list li .product-title,
                                .woocommerce ul.product_list_widget li .product-title,
                                .woocommerce ul.product_list_widget li a,
                                label,
                                .woocommerce div.product .woocommerce-tabs ul.tabs li',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field_responsive(array(
            'type' => 'typography',
            'settings' => 'typography_regular',
            'label' => __('Body', 'rt_domain'),
            'section' => 'general_typography_section',
            'default' => array(
                'font-size' => rt_var('font-size-body'),
                'line-height' => rt_var('line-height-body'),
                'font-family' => rt_var('font-second'),
                'color' => rt_var('second-second'),
                'text-transform' => 'none',
            ),
            'output' => array(
                array(
                    'element' => 'html',
                ),
            ),
            'transport' => 'auto',
        ));

    }

    public function add_color()
    {
        $section = 'general_color_section';

        $this->add_header(array(
            'label' => 'Global',
            'settings' => 'global_color',
            'section' => $section,
            'class' => 'header_main',
        ));

        $this->add_field(array(
            'label' => __('Brand Color', 'rt_domain'),
            'settings' => 'global_brand_color',
            'section' => $section,
            'type' => 'color',
            'choices' => array(
                'alpha' => true,
            ),
            'output' => array(
                array(
                    'element' => '.rt-comment-list__reply:hover,
                                 .woocommerce-MyAccount-navigation li a:hover,
                                 .woocommerce-MyAccount-navigation li.is-active a:hover',
                    'property' => 'background-color',
                ),
            ),
            'transport' => 'auto',
        ));

        $this->add_field_color(array(
            'settings' => 'color_link',
            'label' => 'Link',
            'section' => $section,
            'element' => 'a, .woocommerce-checkout .showlogin',
        ));

        $this->add_field_color(array(
            'settings' => 'color_link_hover',
            'label' => 'Link :Hover',
            'section' => $section,
            'element' => 'a:hover,
                         .rt-post__title a:hover,
                         .rt-widget a:hover',
        ));

    }

    public function add_breadcrumbs()
    {
        $section = 'general_breadcrumbs_section';

        $this->add_header(array(
            'label' => 'Breadcrumbs',
            'settings' => 'page_breadcrumbs',
            'section' => $section,
            'class' => 'breadcrumbs',
        ));

        $this->add_field_link(array(
            'settings' => 'page_breadcrumbs_link',
            'section' => $section,
            'class' => 'breadcrumbs',
            'element' => '.page-header .rt-breadcrumbs a, .page-header .rt-breadcrumbs li::after',
        ));

        $this->add_field_link(array(
            'label' => __('Link :Hover', 'rt_domain'),
            'settings' => 'page_breadcrumbs_link_hover',
            'section' => $section,
            'class' => 'breadcrumbs',
            'element' => '.page-header .rt-breadcrumbs a:hover',
        ));

        $this->add_field_link(array(
            'label' => __('Link :Active', 'rt_domain'),
            'settings' => 'page_breadcrumbs_link_active',
            'section' => $section,
            'class' => 'breadcrumbs',
            'element' => '.page-header .rt-breadcrumbs',
        ));

    }

    public function add_topup()
    {

        $this->add_field(array(
            'type' => 'toggle',
            'settings' => 'nav_gotop',
            'label' => __('Enable Top Up', 'rt_domain'),
            'section' => 'general_topup_section',
            'default' => true,
        ));

        $this->add_field_color(array(
            'settings' => 'topup_color',
            'section' => 'general_topup_section',
            'element' => '.rt-gotop',
            'pseudo' => 'hover',
        ));

        $this->add_field_background(array(
            'settings' => 'topup_background',
            'section' => 'general_topup_section',
            'element' => '.rt-gotop',
            'pseudo' => 'hover',
        ));

        $this->add_field_border_radius(array(
            'settings' => 'topup_border_radius',
            'section' => 'general_topup_section',
            'element' => '.rt-gotop',
        ));
    }

    public function add_pagination_number()
    {

      
    }

    public function add_pagination_loadmore()
    {
    
    }

    /**
     * style button for theme button, ninja form, and contact form 7
     * @return void
     */
    public function add_button_primary()
    {
        $this->add_header(array(
            'label' => 'Button Primary',
            'settings' => 'button_primary',
            'section' => 'general_button_section',
            'class' => 'button_primary',
        ));

        $this->add_field_button(array(
            'settings' => 'button_primary',
            'section' => 'general_button_section',
            'class' => 'button_primary',
            'element' => '.rt-btn--primary,
                        .woocommerce .button,
                        .woocommerce-widget-layered-nav-dropdown__submit',
        ));

    }

    /**
     * style button for theme button, ninja form, and contact form 7
     * @return void
     */
    public function add_button_second()
    {
        $this->add_header(array(
            'label' => 'Button Second',
            'settings' => 'button_second',
            'section' => 'general_button_section',
            'class' => 'button_second',
        ));

        $this->add_field_button(array(
            'settings' => 'button_second',
            'section' => 'general_button_section',
            'class' => 'button_second',
            'element' => '.rt-btn--second
                        .woocommerce-cart .coupon .button,
                        .woocommerce-mini-cart__buttons .button:first-child',
        ));
    }

    public function add_button_action()
    {
       
    }

// end class
}

new General;
