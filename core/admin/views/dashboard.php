<?php
$theme_name = rt_var('product-name');
$theme_version = rt_var('product-version');
$theme_docs = rt_var('product-docs');
$theme_contact = rt_var('product-contact');
$theme_group = rt_var('product-group');
?>

<div class="bulma theme-panel">

    <section class="page-header">
        <h1 class="title"><?php echo rt_var('product-name') ?> Dashboard</h1>
        <h2 class="subtitle">Version <?php echo rt_var('product-version') ?></h2>
    </section>
    <section class="page-info">

        <div class="container is-fluid">

            <div class="columns is-multiline">
                <div class="column is-4">
                    <div class="rta-info-box">
                        <div class="rta-info-box__icon">
                            <i class="fa fa-life-ring"></i>
                        </div>
                        <div class="rta-info-box__body">
                            <h3 class="rta-info-box__title">Butuh Bantuan?</h3>
                            <div class="rta-info-box__content content">
                                <ul>
                                    <li>Jasa pembuatan toko online</li>
                                    <li>Installasi tema</li>
                                    <li>Mempercepat Loading Website</li>
                                    <li>Fix Bug</li>
                                    <li>Support Teknis</li>
                                </ul>
                            </div>
                            <a href="<?php echo $theme_contact ?>" target="_blank" class="button is-info"> Hubungi Kami </a>
                        </div>
                    </div>
                </div>

                <div class="column is-4">
                    <div class="rta-info-box">
                        <div class="rta-info-box__icon">
                            <i class="fa fa-book"></i>
                        </div>
                        <div class="rta-info-box__body">
                            <h3 class="rta-info-box__title">Panduan</h3>
                            <div class="rta-info-box__content content"><p>Pelajari lebih lanjut cara menggunakan Tema Saudagar</p></div>
                            <a href="<?php echo $theme_docs ?>" target="_blank" class="button is-info"> Pelajari Sekarang </a>

                        </div>
                    </div>
                </div>

                <div class="column is-4">
                    <div class="rta-info-box">
                        <div class="rta-info-box__icon">
                            <i class="fa fa-facebook-f"></i>
                        </div>
                        <div class="rta-info-box__body">
                            <h3 class="rta-info-box__title">Group Facebook</h3>
                            <div class="rta-info-box__content content"><p>Diskusi dan berbagi di group Webforia Comunity</p></div>
                            <a href="<?php echo $theme_group ?>" target="_blank" class="button is-info"> Bergabung Sekarang </a>

                        </div>
                    </div>
                </div>



            </div>

        </div>

    </section>
</div>