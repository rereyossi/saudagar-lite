
<?php do_settings_sections("retheme_section");?>

<div class="bulma theme-panel">
    <section class="page-header">
        <h1 class="title"><?php echo rt_var('product-name') ?> Lisensi</h1>
    </section>

    <div class="panel bg-light">

        <div class="panel-heading">
            Lisensi Informasi

            <?php if (rt_is_local()): ?>
                <span class="tag is-success">Local Lisensi</span>
            <?php elseif(rt_is_premium()): ?>
                <span class="tag is-success">Activate</span>
            <?php else: ?>
                <span class="tag is-warning">Deactivate</span>
            <?php endif?>

        </div>

        <div class="panel-block">
            <div>
                <?php if(rt_is_local()): ?>
                  <div style="margin: 15px 0;">Anda sedang menggunakan theme ini di localhost server, semua fitur <strong>Premium</strong> akan aktif pada server lokal. Anda wajib memasukan lisensi ketika di hosting atau server online </div>
                <?php else: ?>
                <div style="margin: 15px 0;">Masukan kunci lisensi Anda untuk dapat menggunakan semua fitur premium, update, dan support. Bila Anda tidak memiliki kunci lisensi silakan kunjungi halaman berikut-> <a target="_blank" href="<?php echo rt_var('product-url')?>">kunci lisensi</a></div>
                <?php endif ?>

                <form action="" method="post">
                    <div class="field">
                        <label class="label">Lisensi Key</label>

                        <?php if (rt_is_local()): ?>
                            <input class="input" id="theme_license_key" name="theme_license_key" type="password" value="localhost" disabled />
                        <?php elseif (rt_is_premium()): ?>
                            <input class="input" type="password" value="<?php echo get_option(rt_var('product-slug', '_key')); ?>" disabled/>
                            <input name="theme_license_key" type="hidden" value="<?php echo get_option(rt_var('product-slug', '_key')); ?>"/>
                        <?php else: ?>
                            <input class="input" id="theme_license_key" name="theme_license_key" type="text" value="<?php echo get_option(rt_var('product-slug', '_key')); ?>"/>
                        <?php endif?>

                    </div>
                    <div class="submit">

                        <?php if (rt_is_local()): ?>
                            <input class="button is-info" name="theme_license_submit" type="submit" value="Activate License" disabled />
                        <?php elseif (rt_is_premium()): ?>
                             <input class="button is-info" name="theme_license_submit" type="submit" value="Deactivate License"/>
                        <?php else: ?>
                            <input class="button is-info" name="theme_license_submit" type="submit" value="Activate License"/>
                        <?php endif?>
                        
                    </div>
                </form>

            </div>

        </div>

    </div>

     

</div>