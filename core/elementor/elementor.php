<?php
namespace Retheme;

use Elementor;

if (!defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly

/**
 * Main Plugin Class
 *
 * Register new elementor widget.
 *
 * @since 1.0.0
 */
class Retheme_Elementor
{

    /**
     * Constructor
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function __construct()
    {
        add_action('elementor/widgets/widgets_registered', [$this, 'on_widgets_registered']);
        add_action('elementor/init', array($this, 'register_category'));
        add_action('elementor/theme/register_locations', array($this, 'register_elementor_locations'));

    }

    /**
     * On Widgets Registered
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function on_widgets_registered()
    {
        $this->includes_part();
        $this->register_widget();
        $this->register_style();
    }

    public function register_category()
    {
        Elementor\Plugin::instance()->elements_manager->add_category(
            'retheme-elements',
            [
                'title' => 'Theme Elements',
                'icon' => 'font',
            ],
            1
        );
    }

    /**
     * Includes
     *
     * @since 1.0.0
     *
     * @access private
     */
    private function includes_part()
    {
 
        require __DIR__ . '/carousel/carousel.php';

        if (rt_is_woocommerce()) {
            require __DIR__ . '/product/product.php';
        }

    }

    public function register_style()
    {
        /**
         * Add CSS on editor Elementor
         */
        add_action('elementor/editor/after_enqueue_scripts', function () {
            wp_enqueue_style('elementor_editor_style', get_template_directory_uri() . '/core/elementor/assets/css/elementor-editor.css');
        });
    }

    /**
     * Register Widget
     *
     * @since 1.0.0
     *
     * @access private
     */
    private function register_widget()
    {
   
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Retheme\Elementor\Carousel());
        
        if (rt_is_woocommerce()) {
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Retheme\Elementor\Products());
        }
    }

    /**
     * Register Elementor Location
     */
    public function register_elementor_locations($elementor_theme_manager)
    {

        $elementor_theme_manager->register_location(
            'sidebar',
            [
                'label' => __('Main Sidebar', 'rt_domain'),
                'multiple' => true,
                'edit_in_content' => false,
            ]
        );

        $elementor_theme_manager->register_location(
            'mobile_drawer',
            [
                'label' => __('Mobile Menu', 'rt_domain'),
                'multiple' => true,
                'edit_in_content' => false,
            ]
        );
        $elementor_theme_manager->register_all_core_location();

    }

// end class
}

new Retheme_Elementor();
