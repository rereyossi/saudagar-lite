=== Saudagar ===
Contributors: Webforia Studio
Requires at least: 4.7
Tested up to: 5.1
Stable tag: 1.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: blog, e-commerce, education, entertainment, food-and-drink, holiday, news, photography, portfolio, accessibility-ready, custom-background, custom-colors, custom-header, custom-logo, editor-style, featured-image-header, featured-images, footer-widgets, full-width-template, post-formats, sticky-post, theme-options, grid-layout, one-column, two-columns, three-columns, left-sidebar, right-sidebar, rtl-language-support, threaded-comments, translation-ready business, construction, corporate, creative, customizable, elementor, hotel, multi purpose, restaurant, shop, woocommerce

== Changelog ==

= 1.2.0 =
* added demo import
* added header builder
* remove code genator classes

= 1.0.0 =
* First release

== Resources ==

Saudagar bundles the following third-party resources:

* [Kirki](http://aristath.github.io/kirki/)
* [Font Awesome Free](https://fontawesome.com)
* [Owl Carousel](https://github.com/OwlCarousel2/OwlCarousel2)